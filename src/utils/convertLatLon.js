import { geocodeByAddress, getLatLng } from "react-places-autocomplete";

export default function convertLatLon(point, type) {
  return new Promise((resolve, redject) => {
    geocodeByAddress(point.directions)
      .then(results => getLatLng(results[0]))
      .then(latlon => {
        console.log(latlon);
        resolve({
          id: point.id,
          coordinates: `POINT(${latlon.lat} ${latlon.lng})`,
          directions: point.directions,
          is_active: 1,
          location_type: type
        });
      })
      .catch(error => redject(error));
  });
}
