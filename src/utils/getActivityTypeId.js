export default function getActivityTypeId(activityType, activityTypes) {
  console.log(activityType);
  const id = activityTypes
    .filter(activity => {
      return activity.value === activityType || activity.text == activityType;
    })
    .map(activity => {
      return activity.id;
    });
  if (id != null) {
    return id[0];
  } else {
    return "could not find Activity";
  }
}

export function getActivityTypeValue(activityType, activityTypes) {
  console.log(activityType);
  const value = activityTypes
    .filter(activity => {
      return activity.value === activityType || activity.text == activityType;
    })
    .map(activity => {
      return activity.value;
    });
  if (value != null) {
    return value[0];
  } else {
    return "could not find Activity";
  }
}
