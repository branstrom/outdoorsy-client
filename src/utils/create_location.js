import axios from "axios";
import convertLatLon from "./convertLatLon";

export default function createLocation(point, type) {
  return new Promise((resolve, redject) => {
    convertLatLon(point, type)
      .then(location => {
        axios.put("/api/activity/location/create", location).then(location => {
          if (location) {
            resolve(location.data);
          }
        });
      })
      .catch(error => redject(error));
  });
}
