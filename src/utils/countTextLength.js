const count = text => {
  let cont = text;
  cont = cont.replace(/<[^>]*>/g, "");
  cont = cont.replace(/\s+/g, "");
  cont = cont.replace(/&nbsp;/g, " ");
  cont = cont.trim();
  return cont.length;
};

export default count;
