import axios from "axios";
import {
  TOGGLE_SESSION_MODAL,
  EDIT_SESSION,
  EDIT_SESSION_SUCCESS,
  EDIT_SESSION_FAIL,
  CREATE_SESSION,
  CREATE_SESSION_SUCCESS,
  CREATE_SESSION_FAIL,
  FETCH_GUIDES,
  FETCH_GUIDES_SUCCESS,
  FETCH_GUIDES_FAIL,
  FETCH_ACTIVIVTY_LOCATION,
  FETCH_ACTIVIVTY_LOCATION_SUCCESS,
  FETCH_ACTIVIVTY_LOCATION_FAIL,
  FETCH_SESSION,
  FETCH_SESSION_SUCCESS,
  FETCH_SESSION_FAIL,
  CLEAR_EDIT_SESSION,
  REMOVE_SESSION,
  REMOVE_SESSION_SUCCESS,
  REMOVE_SESSION_FAIL
} from "./types";
import convertLatLon from "../utils/convertLatLon";
import createLocation from "../utils/create_location";
import moment from "moment";

export function toggleSessionModal(activity_id, session_id) {
  return dispatch => {
    dispatch({
      type: TOGGLE_SESSION_MODAL,
      payload: { activity_id, session_id }
    });
  };
}

export function getActivityLocations(activity_id) {
  return dispatch => {
    dispatch({ type: FETCH_ACTIVIVTY_LOCATION });
    axios
      .get(`/api/activity/location/${activity_id}`)
      .then(locations => {
        console.log(locations);
        if (locations.status === 200) {
          let activityPoints = [];
          let meetingPoints = [];
          locations.data.forEach(location => {
            if (location.location_type === "Activity point") {
              activityPoints.push({
                text: location.directions,
                value: location.id
              });
            } else if (location.location_type === "Meeting point") {
              meetingPoints.push({
                text: location.directions,
                value: location.id
              });
            }
          });
          dispatch({
            type: FETCH_ACTIVIVTY_LOCATION_SUCCESS,
            payload: { activityPoints, meetingPoints }
          });
        } else {
          dispatch({
            type: FETCH_ACTIVIVTY_LOCATION_FAIL,
            payload: locations.error
          });
        }
      })
      .catch(error =>
        dispatch({ type: FETCH_ACTIVIVTY_LOCATION_FAIL, payload: error })
      );
  };
}

export function getGuides(organization_id) {
  return dispatch => {
    dispatch({ type: FETCH_GUIDES });
    axios
      .get(`/api/user/organization/membership/${organization_id}`)
      .then(guides => {
        if (guides) {
          dispatch({
            type: FETCH_GUIDES_SUCCESS,
            payload: guides.data.map(guide => {
              return { text: guide.guide_name, value: guide.guide_id };
            })
          });
        } else {
          dispatch({ type: FETCH_GUIDES_FAIL });
        }
      })
      .catch(error => dispatch({ type: FETCH_GUIDES_FAIL, payload: error }));
  };
}

function formatTime(date, time) {
  return moment(date + " " + time).format("YYYY-MM-DD HH:mm:ss");
}

export function createSession(values, activity_id) {
  return dispatch => {
    dispatch({ type: CREATE_SESSION });
    values.activity_id = activity_id;
    values.start_datetime = formatTime(values.start_date, values.start_time);
    values.end_datetime = formatTime(values.end_date, values.end_time);
    values.status = "Published";
    values.is_active = "1";

    //Check for newly added locations which does not exist in DB yet.
    let locationPromises = [];
    values.locations.forEach(location => {
      if (!location.value) {
        locationPromises.push(createLocation(location.text, "Meeting point"));
      }
    });
    Promise.all(locationPromises).then(newLocations => {
      values.locations = values.locations.map(location => {
        return {
          location_id:
            location.value ||
            newLocations.find(newLocation => {
              return newLocation.directions === location.text;
            }).id,
          time_of_day: location.time_of_day
        };
      });
      console.log(values.locations);

      axios.put("/api/session/create", values).then(response => {
        console.log("session crearte response: ", response);
        if (response.status === 200) {
          dispatch({
            type: CREATE_SESSION_SUCCESS,
            payload: response.data
          });
        } else {
          console.log("FAILED SESSION?");
          dispatch({ type: CREATE_SESSION_FAIL });
        }
      });
    });
  };
}

export function editSession(values) {
  const {
    start_datetime,
    end_datetime,
    max_participants,
    min_participants,
    additional_info,
    activity_id,
    status,
    meeting_point_id,
    activity_point_id
  } = values;
  return dispatch => {
    dispatch({ type: EDIT_SESSION });
    axios.put("/api/session", values).then(response => {
      dispatch({ type: EDIT_SESSION_SUCCESS });
      //dispatch({type: EDIT_SESSION_FAIL});
    });
  };
}

export function removeSession(id) {
  return dispatch => {
    dispatch({ type: REMOVE_SESSION });
    axios
      .delete(`/api/session/${id}`)
      .then(response => {
        console.log(response);
        if (response.status === 200) {
          dispatch({ type: REMOVE_SESSION_SUCCESS, payload: { id } });
        } else {
          dispatch({ type: REMOVE_SESSION_FAIL });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({ type: REMOVE_SESSION_FAIL, payload: error });
      });
  };
}

export function fetchSession(id) {
  return dispatch => {
    dispatch({ type: FETCH_SESSION });
    axios
      .get(`/api/session/show/${id}`)
      .then(response => {
        console.log(response);
        if (response.status === 200) {
          dispatch({ type: FETCH_SESSION_SUCCESS, payload: response.data });
        } else {
          console.log("FETCH FAILED!?");
          dispatch({ type: FETCH_SESSION_FAIL });
        }
      })
      .catch(error => {
        console.log("FAilesd");
        console.log(error);
        dispatch({ type: FETCH_SESSION_FAIL, payload: error });
      });
  };
}

export function clearEditSession() {
  return dispatch => {
    dispatch({ type: CLEAR_EDIT_SESSION });
  };
}
