import axios from "axios";
import assert from "assert";

import {
  TOGGLE_ADD_ACTIVITY_MODAL,
  CREATE_ACTIVITY,
  CREATE_ACTIVITY_SUCCESS,
  CREATE_ACTIVITY_FAIL,
  UPDATE_ACTIVITY,
  UPDATE_ACTIVITY_SUCCESS,
  UPDATE_ACTIVITY_FAIL,
  FETCH_ACTIVITIES,
  FETCH_ACTIVITIES_SUCCESS,
  FETCH_ACTIVITIES_FAIL,
  FETCH_ALL_ACTIVITIES,
  FETCH_ALL_ACTIVITIES_SUCCESS,
  FETCH_ALL_ACTIVITIES_FAIL,
  SET_ACTIVITY_ID,
  SET_SESSION_ID,
  FETCH_ACTIVITY,
  FETCH_ACTIVITY_SUCCESS,
  FETCH_ACTIVITY_FAIL,
  FETCH_ACTIVITY_TYPES,
  FETCH_ACTIVITY_TYPES_SUCCESS,
  FETCH_ACTIVITY_TYPES_FAIL,
  REMOVE_ACTIVITY_SUCCESS,
  REMOVE_ACTIVITY_FAIL,
  REMOVE_ACTIVITY,
  CLEAR_EDIT_ACTIVITY
} from "../actions/types";

import convertLatLon from "../utils/convertLatLon";
var request = require("superagent");

export function toggleActivityModal(id) {
  return dispatch => {
    dispatch({ type: TOGGLE_ADD_ACTIVITY_MODAL, payload: id });
  };
}

function uploadImages(values) {
  console.log("upload Images");
  return new Promise((resolve, redject) => {
    const uploadPromises = [];
    const images = [];
    [0, 1, 2, 3, 4, 5, 6].forEach((value, index) => {
      let image = values[`image_upload_${index}`];
      if (image) {
        images.push({
          image: image,
          media_type: "image",
          placement: index === 0 ? "cover" : "regular"
        });
      }
    });
    console.log(images);
    images.forEach(image => {
      uploadPromises.push(uploadImage(image));
    });
    Promise.all(uploadPromises).then(urls => {
      console.log(urls);
      resolve(urls);
    });
  });
}

function uploadNewImages(newValues, oldValues) {
  console.log("upload new Images");
  return new Promise((resolve, redject) => {
    const uploadPromises = [];
    const images = [];
    [0, 1, 2, 3, 4, 5, 6].forEach((value, index) => {
      let image = newValues[`image_upload_${index}`];
      let oldImage = oldValues[`image_upload_${index}`];
      console.log(image);
      console.log(oldImage);
      if (image && oldImage && image.preview !== oldImage.preview) {
        console.log("UPDATING: ", image);
        images.push({
          id: oldImage.id,
          image: image,
          media_type: "image",
          placement: index === 0 ? "cover" : "regular"
        });
      }
      if (image && !oldImage) {
        images.push({
          image: image,
          media_type: "image",
          placement: index === 0 ? "cover" : "regular"
        });
      }
    });
    images.forEach(image => {
      uploadPromises.push(uploadImage(image));
    });
    Promise.all(uploadPromises).then(urls => {
      console.log(urls);
      resolve(urls);
    });
  });
}

function uploadImage(image) {
  console.log("upload: ", image);
  return new Promise((resolve, redject) => {
    const url = `/api/upload`;
    const req = request.post(url);
    req.attach("image", image.image);
    req.end((err, res) => {
      if (err) redject(err);
      console.log(res);
      resolve({
        id: image.id,
        media_type: image.media_type,
        placement: image.placement,
        media_url: res.body.imageUrl
      });
    });
  });
}

function prepareValues(values) {
  return new Promise((resolve, redject) => {
    let locations = [];
    const activityPromises = [];
    const meetingPromises = [];

    values.meeting_point_description &&
      values.meeting_point_description.map((value, index) => {
        console.log(value);
        meetingPromises.push(convertLatLon(value, "Meeting point"));
      });
    values.activity_point_description &&
      values.activity_point_description.map((value, index) => {
        activityPromises.push(convertLatLon(value, "Activity point"));
      });
    Promise.all(activityPromises).then(result => {
      result.map(location => {
        locations.push(location);
      });
      Promise.all(meetingPromises).then(result => {
        result.map(location => {
          locations.push(location);
        });
        let items = [];
        values.included &&
          values.included.forEach(item => {
            items.push({
              id: item.id,
              item_type: "Included",
              name: item.name
            });
          });
        values.required &&
          values.required.forEach(item => {
            items.push({
              id: item.id,
              item_type: "Required",
              name: item.name
            });
          });
        values = Object.assign({}, values, {
          guide_id: 1,
          locations,
          items,
          status: "Active"
        });
        console.log(values);
        resolve(values);
      });
    });
  });
}

function prepareUpdateValues(values, oldValues) {
  return new Promise((resolve, redject) => {
    const activityPromises = [];
    const meetingPromises = [];
    values.locations = [];

    values.meeting_point_description.map((value, index) => {
      if (value.id) {
        values.locations.push({
          ...value,
          coordinates: `POINT(${value.coordinates.x} ${value.coordinates.y})`
        });
      } else {
        meetingPromises.push(convertLatLon(value, "Meeting point"));
      }
    });
    values.activity_point_description.map((value, index) => {
      if (value.id) {
        values.locations.push({
          ...value,
          coordinates: `POINT(${value.coordinates.x} ${value.coordinates.y})`
        });
      } else {
        activityPromises.push(convertLatLon(value, "Activity point"));
      }
    });
    console.log(values.locations);
    Promise.all(activityPromises).then(result => {
      result.map(location => {
        values.locations.push(location);
      });
      Promise.all(meetingPromises).then(result => {
        result.map(location => {
          values.locations.push(location);
        });
        console.log(values.locations);
        let items = [];
        values.included.forEach(item => {
          items.push({
            id: item.id,
            item_type: "Included",
            name: item.name
          });
        });
        values.required.forEach(item => {
          items.push({
            id: item.id,
            item_type: "Required",
            name: item.name
          });
        });
        if (values.locations.length < oldValues.locations.length) {
          values.locations = addMissingLocation(
            values.locations,
            oldValues.locations
          );
        }
        console.log(values.items);
        values.items = items;
        if (values.items.length < oldValues.items.length) {
          values.items = addMissing(values.items, oldValues.items);
        }
        console.log(values.items);
        resolve(values);
      });
    });
  });
}

export function createActivity(values) {
  return dispatch => {
    dispatch({ type: CREATE_ACTIVITY });
    prepareValues(values)
      .then(newValues => {
        console.log(newValues);
        uploadImages(newValues).then(media => {
          console.log(media);
          newValues.media = media;
          axios
            .put("/api/activity/create", newValues)
            .then(response => {
              if (response.status === 200) {
                dispatch({
                  type: CREATE_ACTIVITY_SUCCESS,
                  payload: response.data
                });
              } else {
                dispatch({ type: CREATE_ACTIVITY_FAIL });
              }
            })
            .catch(error =>
              dispatch({ type: CREATE_ACTIVITY_FAIL, payload: error })
            );
        });
      })
      .catch(error => {
        console.log(error);
        dispatch({ type: CREATE_ACTIVITY_FAIL, payload: error });
      });
  };
}

//
export function updateActivity(activity_id, values, oldValues) {
  return dispatch => {
    //check if images changed, if changed, update the url for that one and send to server.
    //check if activity_point_description and meeting_point_description changed, if changed send the updated location-array.
    // included & required changed, if changed, send all of it.
    console.log(oldValues);
    if (Object.keys(oldValues).length === 0) return;
    dispatch({ type: UPDATE_ACTIVITY });
    prepareUpdateValues(values, oldValues)
      .then(newValues => {
        console.log(newValues);
        uploadNewImages(newValues, oldValues).then(media => {
          console.log(media);
          newValues.media = media;
          console.log(newValues);
          axios
            .put("/api/activity/update/" + activity_id, newValues)
            .then(response => {
              if (response.status === 200) {
                dispatch({
                  type: UPDATE_ACTIVITY_SUCCESS,
                  payload: response.data
                });
              } else {
                dispatch({ type: UPDATE_ACTIVITY_FAIL });
              }
            })
            .catch(error =>
              dispatch({ type: UPDATE_ACTIVITY_FAIL, payload: error })
            );
        });
      })
      .catch(error => {
        console.log(error);
        dispatch({ type: UPDATE_ACTIVITY_FAIL, payload: error });
      });
  };
}

function exist(id, array) {
  let exists = false;
  array.forEach(value => {
    if (value.id === id) {
      exists = true;
      return;
    }
  });
  return exists;
}

function addMissingLocation(newValues, oldValues) {
  oldValues.forEach(value => {
    if (value.id && !exist(value.id, newValues)) {
      newValues.push({
        id: value.id,
        directions: value.directions,
        coordinates: `POINT(${value.coordinates.x} ${value.coordinates.y})`,
        is_active: 1,
        location_type: value.type,
        is_deleted: 1
      });
    }
  });
  return newValues;
}

function addMissing(newValues, oldValues) {
  oldValues.forEach(value => {
    if (!exist(value.id, newValues)) {
      newValues.push({
        id: value.id,
        name: value.name,
        is_deleted: 1
      });
    }
  });
  return newValues;
}

export function fetchSessionActivities(organizationId) {
  return dispatch => {
    dispatch({ type: FETCH_ACTIVITIES });
    axios.get(`/api/session/organization/${organizationId}`).then(response => {
      console.log(response.data);
      if (response.status === 200) {
        dispatch({ type: FETCH_ACTIVITIES_SUCCESS, payload: response.data });
      } else {
        dispatch({ type: FETCH_ACTIVITIES_FAIL });
      }
      console.log(response);
    });
  };
}

export function fetchAllActivities(organizationId) {
  return dispatch => {
    dispatch({ type: FETCH_ALL_ACTIVITIES });
    axios.get(`/api/activity`).then(response => {
      console.log(response.data);
      const activities = response.data.filter(activity => {
        return activity.organization_id === organizationId;
      });
      if (response.status === 200) {
        dispatch({ type: FETCH_ALL_ACTIVITIES_SUCCESS, payload: activities });
      } else {
        dispatch({ type: FETCH_ALL_ACTIVITIES_FAIL });
      }
    });
  };
}

export function fetchActivity(id) {
  return dispatch => {
    dispatch({ type: FETCH_ACTIVITY });
    axios
      .get(`/api/activity/show/${id}`)
      .then(response => {
        console.log(response);
        if (response.status === 200) {
          dispatch({ type: FETCH_ACTIVITY_SUCCESS, payload: response.data });
        } else {
          console.log("FETCH FAILED!?");
          dispatch({ type: FETCH_ACTIVITY_FAIL });
        }
      })
      .catch(error => {
        console.log("FAilesd");
        console.log(error);
        dispatch({ type: FETCH_ACTIVITY_FAIL, payload: error });
      });
  };
}

export function removeActivity(id) {
  return dispatch => {
    dispatch({ type: REMOVE_ACTIVITY });
    axios
      .delete(`/api/activity/${id}`)
      .then(response => {
        console.log(response);
        if (response.status === 200) {
          dispatch({ type: REMOVE_ACTIVITY_SUCCESS, payload: { id } });
        } else {
          dispatch({ type: REMOVE_ACTIVITY_FAIL });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({ type: REMOVE_ACTIVITY_FAIL, payload: error });
      });
  };
}

export function setActivity(id) {
  return dispatch => {
    dispatch({ type: SET_ACTIVITY_ID, payload: id });
  };
}

export function setSession(id) {
  return dispatch => {
    dispatch({ type: SET_SESSION_ID, payload: id });
  };
}

export function clearEditActivity() {
  return dispatch => {
    dispatch({ type: CLEAR_EDIT_ACTIVITY });
  };
}

export function fetchActivityTypes() {
  return dispatch => {
    dispatch({ type: FETCH_ACTIVITY_TYPES });
    axios
      .get(`/api/activity/types`)
      .then(response => {
        if (response.status === 200) {
          console.log();
          dispatch({
            type: FETCH_ACTIVITY_TYPES_SUCCESS,
            payload: response.data.map(activity => {
              return {
                id: activity.id,
                text: activity.activity_type_name,
                value: activity.activity_type_name,
                category: activity.category,
                vat: activity.vat
              };
            })
          });
        } else {
          dispatch({ type: FETCH_ACTIVITY_TYPES_FAIL });
        }
      })
      .catch(error => {
        dispatch({ type: FETCH_ACTIVITY_TYPES_FAIL, payload: error });
      });
  };
}
