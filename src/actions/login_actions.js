import {
  AUTH_USER_FAIL,
  AUTH_USER,
  AUTH_USER_SUCCESS,
  VALIDATE_USER,
  VALIDATE_USER_SUCCESS,
  VALIDATE_USER_FAIL,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER,
  SIGNUP_USER,
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAIL
} from "./types";
import axios from "axios";

export function loginWithToken(token) {
  return dispatch => {
    dispatch({ type: AUTH_USER });
    axios
      .post("/api/auth/login", {
        token
      })
      .then(response => {
        if (response.status === 200) {
          dispatch({ type: AUTH_USER_SUCCESS, payload: response.data.name });
        } else {
          dispatch({ type: AUTH_USER_FAIL, payload: token });
        }
      });
  };
}

export function signup(values) {
  return dispatch => {
    dispatch({ type: SIGNUP_USER });
    axios
      .put("/api/user/guide/create", values)
      .then(response => {
        if (response.status === 200) {
          dispatch({ type: SIGNUP_USER_SUCCESS, payload: response.data.name });
        } else {
          dispatch({ type: SIGNUP_USER_FAIL, payload: response });
        }
      })
      .catch(error =>
        dispatch({ type: SIGNUP_USER_FAIL, payload: error.response })
      );
  };
}

export function login(email, password) {
  return dispatch => {
    dispatch({ type: AUTH_USER });
    axios
      .post("/api/auth/login", {
        email,
        password
      })
      .then(response => {
        console.log(response.data);
        if (response.status === 200) {
          dispatch({ type: AUTH_USER_SUCCESS, payload: response.data });
        } else {
          dispatch({ type: AUTH_USER_FAIL, payload: response });
        }
      })
      .catch(error => {
        dispatch({
          type: AUTH_USER_FAIL,
          payload: "Email and password does not match"
        });
      });
  };
}

export function isLoggedIn() {
  return dispatch => {
    dispatch({ type: VALIDATE_USER });
    axios
      .get("/api/auth/validate")
      .then(response => {
        if (response.status === 200) {
          dispatch({
            type: VALIDATE_USER_SUCCESS,
            payload: response.data
          });
        } else {
          dispatch({ type: VALIDATE_USER_FAIL, payload: response });
        }
      })
      .catch(error => {
        console.log(error.request);
        dispatch({ type: VALIDATE_USER_FAIL });
      });
  };
}

export function logout() {
  return dispatch => {
    dispatch({ type: LOGOUT_USER });
    axios
      .get("/api/auth/logout")
      .then(response => {
        console.log("RESPONSE!");
        if (response.status === 200) {
          dispatch({ type: LOGOUT_USER_SUCCESS });
        }
      })
      .catch(error => {
        console.log(error.request);
      });
  };
}
