import axios from "axios";
/**
 *
 * @type {{withCredentials: boolean}} - send cookes with requies, our "authenticated" token is an HTTP cookie.
 */
const config = {
  withCredentials: true
};

export default class ApiAuth {
  static getLoggedInUser() {
    return axios.get("/api/loggedin", config);
  }
}
