const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default function validate(values) {
  const errors = {};
  if (!values.email || !emailRegex.test(values.email)) {
    errors.email = "Please enter a valid email";
  }
  if (!values.password) {
    errors.password = "Please enter a password";
  }
  return errors;
}
