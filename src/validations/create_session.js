export default function validate(values) {
  const errors = {};
  if (!values.currency) {
    errors.currency = "Please a currency.";
  }
  if (!values.start_date) {
    errors.start_date = "Please enter a start date";
  }
  if (!values.start_time) {
    errors.start_time = "Please enter a start time.";
  }
  if (!values.end_date) {
    errors.end_date = "Please enter a start date and time.";
  }
  if (!values.end_time) {
    errors.start_time = "Please enter a end time.";
  }
  if (
    values.end_date === values.start_date &&
    values.end_time <= values.start_time
  ) {
    errors.end_date = "Please choose a time occuring after the start.";
  }
  if (!values.amount) {
    errors.amount = "Please enter a price.";
  }
  if (!values.max_participants) {
    errors.max_participants =
      "Please enter the maximum number of participants.";
  }
  if (!values.min_participants) {
    errors.min_participants =
      "Please enter the minimum number of participants.";
  }
  if (!values.activity_point_id) {
    errors.activity_point_id = "Please choose a location for the session";
  }
  if (values.activity_point_id == "Choose location") {
    errors.activity_point_id = "Please choose a location for the session";
  }
  if (!values.guide_id) {
    errors.guide_id = "Please choose a guide for the session";
  }
  if (!values.locations || values.locations.length === 0) {
    errors.locations = { _error: "Please choose at least one meeting point." };
  } else {
    values.locations.forEach((location, index) => {
      if (!location || !location.time_of_day || !location.text) {
        errors.locations = {
          _error: "Please enter a time and place for all locations"
        };
      }
    });
  }
  console.log("Validated form, errors: ", errors);
  return errors;
}
