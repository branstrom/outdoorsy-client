const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default function validate(values) {
  const errors = {};
  if (!values.email || !emailRegex.test(values.email)) {
    errors.email = "Please enter a valid email";
  }
  if (!values.password) {
    errors.password = "Please enter a password";
  }
  if (!values.first_name) {
    errors.first_name = "Please enter a first name";
  }
  if (!values.last_name) {
    errors.last_name = "Please enter a last name";
  }
  if (!values.guide_mobile_phone) {
    errors.guide_mobile_phone = "Please enter a mobile phone number";
  }
  if (!values.organization_name) {
    errors.organization_name = "Please enter an organization name";
  }
  if (!values.day) {
    errors.day = "Please enter a day of birth";
  }
  if (!values.month) {
    errors.month = "Please enter a month of birth";
  }
  if (!values.year) {
    errors.year = "Please enter a year of birth";
  }
  if (!values.line1) {
    errors.line1 = "Please enter a street";
  }
  if (!values.postal_code) {
    errors.postal_code = "Please enter a postal code";
  }
  if (!values.city) {
    errors.city = "Please enter a city";
  }
  if (!values.terms) {
    errors.terms = "Please accept the terms";
  }
  return errors;
}
