import count from "../utils/countTextLength";

export default function validate(values) {
  const errors = {};
  if (!values.activity_name) {
    errors.activity_name = "Please enter an activity name";
  }
  if (!values.activity_category) {
    errors.activity_category = "Please enter an activity type";
  }
  if (!values.activity_type) {
    errors.activity_type = "Please enter an activity type";
  }
  if (!values.level) {
    errors.level = "Please enter an activity level";
  }
  if (!values.detailed_description) {
    errors.detailed_description = "Please enter an activity description";
  }
  if (count(values.detailed_description) < 31) {
    errors.detailed_description =
      "Please enter a description longer than 30 characters";
  }
  if (count(values.detailed_description) > 3000) {
    errors.detailed_description =
      "Please enter a description shorter than 3000 characters";
  }
  if (!values.description) {
    errors.description = "Please enter an activity description";
  }
  if (count(values.description) < 31) {
    errors.description = "Please enter a description longer than 30 characters";
  }
  if (count(values.description) > 200) {
    errors.description =
      "Please enter a description shorter than 200 characters";
  }
  if (!values.activity_point_description.length) {
    errors.activity_point_description = {
      _error: "Please enter an activity point"
    };
  }
  if (values.activity_point_description.length > 7) {
    errors.activity_point_description = {
      _error: "You can't add more than 7 activity points."
    };
  }
  if (!values.meeting_point_description.length) {
    errors.meeting_point_description = {
      _error: "Please enter an meeting point"
    };
  }
  if (values.meeting_point_description.length > 7) {
    errors.meeting_point_description = {
      _error: "You can't add more than 7 meeting points."
    };
  }
  if (!values.image_upload_0) {
    errors.image_upload_0 = "Upload at least one picture";
  }
  if (values.included.length === 0) {
    errors.included = { _error: "Add at least one item" };
  }
  if (values.required.length === 0) {
    errors.required = { _error: "Add at least one item" };
  }
  if (!values.color) {
    errors.color = "Please choose a color";
  }

  console.log("Validated form, errors: ", errors);
  return errors;
}
