import React, { Component } from "react";
import { connect } from "react-redux";
import DraggableZone from "../components/draggable_zone";
import ActivityList from "../components/activity_list";
import Sessions from "../components/sessions";
import CreateActivity from "../components/create_activity";
import CreateSession from "../components/create_session";
import Modal from "../components/modal";
import {
  toggleActivityModal,
  createActivity,
  updateActivity,
  fetchSessionActivities,
  fetchAllActivities,
  setActivity,
  setSession,
  fetchActivity,
  removeActivity,
  fetchActivityTypes
} from "../actions/activities_actions";
import {
  toggleSessionModal,
  createSession,
  editSession,
  getGuides,
  getActivityLocations,
  fetchSession,
  removeSession,
  clearEditSession
} from "../actions/sessions_actions";

class Activities extends Component {
  constructor(props) {
    super(props);
    this.noSessions = this.noSessions.bind(this);
    this.showActivityDelete = this.showActivityDelete.bind(this);
    this.showSessionDelete = this.showSessionDelete.bind(this);
  }
  componentWillMount() {
    const { organizationId } = this.props;
    if (!this.props.fetching && organizationId) {
      this.props.fetchSessionActivities(organizationId);
    }
    if (!this.props.fetchingAll && organizationId) {
      this.props.fetchAllActivities(organizationId);
    }
    if (this.props.types.length === 0) {
      this.props.fetchActivityTypes();
    }
  }
  componentWillReceiveProps(nextProps) {
    const { organizationId } = nextProps;
    if (organizationId && !this.props.organizationId) {
      this.props.fetchSessionActivities(organizationId);
      this.props.fetchAllActivities(organizationId);
    }
    if (
      this.props.session.creating &&
      !nextProps.session.creating &&
      !nextProps.session.error
    ) {
      this.props.fetchSessionActivities(organizationId);
    }
  }
  noSessions() {
    console.log(Object.values(this.props.activities) < 1);
    return Object.values(this.props.activities) < 1;
  }
  showActivityDelete() {
    const { modalActivityId, activities } = this.props;
    if (activities && modalActivityId && activities[modalActivityId]) {
      const sessions = Object.values(activities[modalActivityId].sessions);
      return sessions.length === 0;
    }
    return true;
  }
  showSessionDelete() {
    const {
      modalActivityId,
      activityId,
      activities,
      modalSessionId
    } = this.props;
    if (
      activities &&
      activityId &&
      activities[activityId] &&
      activities[activityId].sessions &&
      modalSessionId &&
      activities[activityId].sessions[modalSessionId] &&
      activities[activityId].sessions[modalSessionId].participants
    ) {
      const participants = Object.values(
        activities[activityId].sessions[modalSessionId].participants
      );

      return participants.length === 0;
    }
    return true;
  }

  render() {
    if (this.props.fetching || this.props.fetchingAll) {
      return <div />;
    }
    return (
      <div className="columns">
        <div className="column is-one-quarter">
          <ActivityList
            toggleActivityModal={this.props.toggleActivityModal}
            toggleSessionModal={this.props.toggleSessionModal}
            activities={this.props.allActivities}
          />
        </div>

        <div className="column is-three-quarter width-100-percent">
          <DraggableZone noSessions={this.noSessions()}>
            <Sessions
              activities={this.props.activities}
              loading={this.props.fetching}
              setActivity={this.props.setActivity}
              setSession={this.props.setSession}
              sessionId={this.props.sessionId}
              activityId={this.props.activityId}
              toggleActivityModal={this.props.toggleActivityModal}
              toggleSessionModal={this.props.toggleSessionModal}
            />
          </DraggableZone>
        </div>

        <Modal isActive={this.props.addActivityModal}>
          <CreateActivity
            toggleActivityModal={this.props.toggleActivityModal}
            modalActivityId={this.props.modalActivityId}
            fetchActivity={this.props.fetchActivity}
            createActivity={this.props.createActivity}
            updateActivity={this.props.updateActivity}
            removeActivity={this.props.removeActivity}
            oldValues={this.props.oldValues}
            types={this.props.types}
            categories={this.props.categories}
            loading={this.props.creating}
            showDelete={this.showActivityDelete}
            removing={this.props.removing}
            updating={this.props.updating}
            errors={this.props.errors}
          />

        </Modal>

        <Modal isActive={this.props.sessionModal}>
          <CreateSession
            toggleSessionModal={this.props.toggleSessionModal}
            modalSessionId={this.props.modalSessionId}
            session={this.props.session.session}
            activities={this.props.allActivities}
            createSession={this.props.createSession}
            removeSession={this.props.removeSession}
            removing={this.props.removing}
            editSession={this.props.editSession}
            fetchSession={this.props.fetchSession}
            organizationId={this.props.organizationId}
            activity_id={this.props.session.activity_id}
            getGuides={this.props.getGuides}
            guides={this.props.guides}
            showDelete={this.showSessionDelete}
            activityPoints={this.props.session.activityPoints}
            meetingPoints={this.props.session.meetingPoints}
            getActivityLocations={this.props.getActivityLocations}
            clearEditSession={this.props.clearEditSession}
            loading={this.props.creating}
          />
          <button
            className="modal-close is-large"
            onClick={() => this.props.toggleSessionModal()}
          />
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    addActivityModal: state.activities.addActivityModal,
    modalActivityId: state.activities.modalActivityId,
    modalSessionId: state.session.modalSessionId,
    sessionModal: state.session.sessionModal,
    session: state.session,
    creating: state.activities.creating || state.session.creating,
    updating: state.activities.updating,
    activities: state.activities.activities,
    allActivities: state.activities.allActivities,
    fetching: state.activities.fetching,
    fetchingAll: state.activities.fetchingAll,
    activityId: state.activities.activityId,
    sessionId: state.activities.sessionId,
    errors: state.activities.errors,
    organizationId: state.auth.organizationId,
    guides: state.session.guides,
    oldValues: state.editActivity,
    removing: state.removing,
    types: state.activities.types,
    categories: state.activities.categories
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getGuides: (...props) => dispatch(getGuides(...props)),
    toggleActivityModal: (...props) => dispatch(toggleActivityModal(...props)),
    toggleSessionModal: (...props) => dispatch(toggleSessionModal(...props)),
    setActivity: (...props) => dispatch(setActivity(...props)),
    setSession: (...props) => dispatch(setSession(...props)),
    createActivity: (...props) => dispatch(createActivity(...props)),
    updateActivity: (...props) => dispatch(updateActivity(...props)),
    fetchActivity: (...props) => dispatch(fetchActivity(...props)),
    fetchSessionActivities: (...props) =>
      dispatch(fetchSessionActivities(...props)),
    fetchAllActivities: (...props) => dispatch(fetchAllActivities(...props)),
    createSession: (...props) => dispatch(createSession(...props)),
    editSession: (...props) => dispatch(editSession(...props)),
    getActivityLocations: (...props) => dispatch(getActivityLocations(props)),
    removeActivity: (...props) => dispatch(removeActivity(props)),
    removeSession: (...props) => dispatch(removeSession(props)),
    fetchSession: (...props) => dispatch(fetchSession(props)),
    fetchActivityTypes: (...props) => dispatch(fetchActivityTypes(props)),
    clearEditSession: (...props) => dispatch(clearEditSession(props))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Activities);
