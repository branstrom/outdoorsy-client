import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

class EditProfile extends Component {
  render() {
    return <div>EditProfile</div>;
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated
  };
}

export default withRouter(connect(mapStateToProps)(EditProfile));
