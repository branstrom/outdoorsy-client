import React from "react";
import video from "../video/mount_yasur.mp4";
import { Route } from "react-router-dom";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import { withRouter } from "react-router-dom";
import { FormInput } from "../components/form_fields/form_input";
import { Terms } from "../components/form_fields/terms_of_service";
import { Button } from "../components/elements/button";
import { Label } from "../components/form_fields/label";
import { signup } from "../actions/login_actions";
import { TermsOfService } from "../components/terms_of_service";
import { GoogleSignin } from "../components/google_signin";
import validate from "../validations/signup";
import axios from "axios";
import logo from "../img/outdoorsy_logo.png";

/**
 * Sign in page with full screen video background.
 */
class SignUp extends React.Component {
  constructor(props) {
    super(props);
  }

  onSubmit(values) {
    this.props.signup(values);
    console.log("submit");
  }

  render() {
    console.log(this.props.loading);
    const { handleSubmit, signupSuccess } = this.props;
    return (
      <section className="hero is-fullheight transparent">
        <img src={logo} alt="Outdoorsy" className="logo" />
        <div className="hero-body login-box">
          <div className="login-container">
            {!signupSuccess && (
              <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <Field
                  label="Email"
                  name="email"
                  type="email"
                  component={FormInput}
                  placeholder="@ Email Address"
                  style="aw-form-input"
                />
                <Field
                  label="First name"
                  name="first_name"
                  component={FormInput}
                  placeholder="First Name"
                  style="aw-form-input"
                />
                <Field
                  label="Last Name"
                  name="last_name"
                  component={FormInput}
                  placeholder="Last Name"
                  style="aw-form-input"
                />
                <Field
                  label="Last Name"
                  name="guide_mobile_phone"
                  component={FormInput}
                  placeholder="Phone number"
                  style="aw-form-input"
                />
                <Field
                  label="Company Name"
                  name="organization_name"
                  component={FormInput}
                  placeholder="Organization name"
                  style="aw-form-input"
                />
                <Field
                  label="Day"
                  name="day"
                  component={FormInput}
                  placeholder="Day of birth (XX)"
                  style="aw-form-input"
                />
                <Field
                  label="Month"
                  name="month"
                  component={FormInput}
                  placeholder="Month of birth (XX)"
                  style="aw-form-input"
                />
                <Field
                  label="Year"
                  name="year"
                  component={FormInput}
                  placeholder="Year of birth (XXXX)"
                  style="aw-form-input"
                />
                <Field
                  label="Street"
                  name="line1"
                  component={FormInput}
                  placeholder="Street XX"
                  style="aw-form-input"
                />
                <Field
                  label="Postal code"
                  name="postal_code"
                  component={FormInput}
                  placeholder="Postal code (XXXXX)"
                  style="aw-form-input"
                />
                <Field
                  label="City"
                  name="city"
                  component={FormInput}
                  placeholder="City"
                  style="aw-form-input"
                  autoComplete="off"
                />
                <Field
                  label="Password"
                  name="password"
                  type="password"
                  component={FormInput}
                  placeholder="Password"
                  autoComplete="new-password"
                  style="aw-form-input"
                />
                <Field name="terms" component={Terms} />
                <Button
                  type="submit"
                  className="button aw-button"
                  text="Register Account"
                  loading={this.props.loading}
                  disabled={this.props.loading}
                />

                {/* <p className="margin-bottom-sm margin-top-sm">
                  Or use Google login to create an account:
                </p>
                <GoogleSignin />
                <TermsOfService /> */}
              </form>
            )}
            {signupSuccess && <div>Registration successful</div>}
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    signupSuccess: state.auth.signupSuccess
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signup: (...props) => dispatch(signup(...props))
  };
};

export default reduxForm({
  validate,
  form: "signup"
})(withRouter(connect(mapStateToProps, mapDispatchToProps)(SignUp)));
