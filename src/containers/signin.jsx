import React from "react";
import video from "../video/mount_yasur.mp4";
import { Route, Link, Switch } from "react-router-dom";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import { withRouter } from "react-router-dom";
import { FormInput } from "../components/form_fields/form_input";
import { login, loginWithToken } from "../actions/login_actions";
import SignUp from "./signup";
import { Label } from "../components/form_fields/label";
import { Error } from "../components/form_fields/error";
import { Button } from "../components/elements/button";
import { GoogleSignin } from "../components/google_signin";
import logo from "../img/outdoorsy_logo.png";
import validate from "../validations/login";
import axios from "axios";

/**
 * Sign in page with full screen video background.
 */
class Signin extends React.Component {
  constructor(props) {
    super(props);
    this.onSignIn = this.onSignIn.bind(this);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    axios.get("/api/user/google").then(response => {
      console.log(response);
    });
  }

  onSubmit(values) {
    this.props.login(values.email, values.password);
    console.log("submit");
  }

  onSignIn(googleUser) {
    console.log("onSignin!?");
    const token = googleUser.getAuthResponse().id_token;
    this.props.loginWithToken(token);
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <section className="hero is-fullheight transparent">
        <img src={logo} alt="Outdoorsy" className="logo" />
        <div className="hero-body login-box">
          <div className="login-container">
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <Field
                label="Email"
                name="email"
                component={FormInput}
                placeholder="@ Email Address"
                style="aw-form-input"
              />
              <Field
                label="Password"
                name="password"
                type="password"
                placeholder="Password"
                component={FormInput}
                style="aw-form-input"
              />
              <Button
                type="submit"
                className="button aw-button"
                text="Login"
                disabled={this.props.loading}
                loading={this.props.loading}
              />
              <Error message={this.props.error} />
            </form>
            <br />
            {/* <GoogleSignin /> */}
            <br />
            {/* <Link to="/signup">
              <h0>Create an account</h0>
            </Link> */}
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    authenticated: state.auth.authenticated,
    loading: state.auth.loading,
    error: state.auth.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loginWithToken: token => {
      dispatch(loginWithToken(token));
    },
    login: (...props) => dispatch(login(...props))
  };
};

export default reduxForm({
  validate,
  form: "login"
})(withRouter(connect(mapStateToProps, mapDispatchToProps)(Signin)));
