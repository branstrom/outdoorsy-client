import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

class AccountSettings extends Component {
  render() {
    return <div>AccountSettings</div>;
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated
  };
}

export default withRouter(connect(mapStateToProps)(AccountSettings));
