import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import Header from "../components/header";
import RequireAuth from "../components/auth/require_auth";
import Welcome from "../components/welcome";
import Activities from "./activities";
import CreateActivity from "../components/create_activity";
import AccountSettings from "./account_settings";
import EditProfile from "./edit_profile";
import { logout } from "../actions/login_actions";

class MainApp extends Component {
  componentWillMount() {
    if (!this.props.authenticated) {
      console.log("# MainApp cwm, signing in...");
    } else {
      console.log("# MainApp cwm, already signed in :)");
    }
  }

  render() {
    return (
      <div>
        <Header logout={this.props.logout} />
        <div className="main-container">
          <div className="aw-header-app-container">
            <Switch>
              <Route path="/app" component={RequireAuth(Activities)} />
              <Route
                path="/app/activities"
                component={RequireAuth(Activities)}
              />
              <Route
                path="/app/account"
                component={RequireAuth(AccountSettings)}
              />
              <Route path="/app/profile" component={RequireAuth(EditProfile)} />
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated
  };
}

const mapDispatchToProps = dispatch => {
  return {
    logout: (...props) => dispatch(logout(...props))
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(MainApp)
);
