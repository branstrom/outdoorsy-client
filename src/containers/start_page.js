import React from "react";
import video from "../video/mount_yasur.mp4";
import { Route, Link, Switch } from "react-router-dom";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import { withRouter } from "react-router-dom";
import { FormInput } from "../components/form_fields/form_input";
import { login, loginWithToken } from "../actions/login_actions";
import SignUp from "./signup";
import SignIn from "./signin";
import { Label } from "../components/form_fields/label";
import axios from "axios";
import logo from "../img/outdoorsy_logo.png";

/**
 * Sign in page with full screen video background.
 */
class StartPage extends React.Component {
  render() {
    if (this.props.validating) {
      return <div />;
    }
    return (
      <section className="hero is-fullheight transparent">
        <img src={logo} alt="Outdoorsy" className="logo" />
        <div className="hero-body login-box">
          <Route path="/" component={SignIn} />
          <Route path="/signup" component={SignUp} />
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    validating: state.auth.validating
  };
};

export default withRouter(connect(mapStateToProps)(StartPage));
