import React from "react";
let serverUrl;

if (window.location.href.startsWith("http://localhost:3000")) {
  serverUrl = "http://localhost:3001";
} else {
  serverUrl = "";
}

export const GoogleSignin = () => {
  return (
    <a
      href={`${serverUrl}/api/auth/google`}
      className="btn btn-danger"
      onClick={this.onClick}
    >
      <span className="fa fa-google-plus" /> Google
    </a>
  );
};
