import React from "react";
import "bulma/sass/elements/button.sass";

export default () =>
  <div>
    <a className="button">Button</a>
    <h1>Welcome to Outdoorsy!</h1>
  </div>;
