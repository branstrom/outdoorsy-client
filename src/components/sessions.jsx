import React, { Component } from "react";
import ReactTable from "react-table";
import dateformat from "dateformat";
import ColorSymbol from "./elements/color_symbol";
import moment from "moment";

const ParticipantsTable = props => {
  const { participants, loading, getColumnWidth } = props;
  return (
    <ReactTable
      data={participants}
      defaultPageSize={participants.length < 3 ? 3 : participants.length}
      noDataText="No available participants"
      columns={[
        {
          Header: "First name",
          accessor: "first_name",
          minWidth: getColumnWidth("First name", "first_name", participants)
        },
        {
          Header: "Last name",
          accessor: "last_name",
          minWidth: getColumnWidth("Last name", "last_name", participants)
        },
        {
          id: "age",
          Header: "Age",
          accessor: row => {
            return row.age ? moment().diff(row.age, "years") : "";
          },
          width: 100
        },
        {
          Header: "Mobile",
          accessor: "mobile_number",
          minWidth: 100
        },
        {
          Header: "Email",
          id: "email",
          accessor: row => {
            return row.email ? (
              <a href={`mailto:${row.email}`}>{row.email}</a>
            ) : (
              ""
            );
          },
          minWidth: getColumnWidth("Email", "email", participants)
        },
        {
          Header: "Gender",
          accessor: "gender",
          minWidth: 60
        },
        {
          Header: "Height",
          accessor: "height",
          minWidth: getColumnWidth("Height", "height", participants)
        },
        {
          Header: "Weight",
          accessor: "weight",
          minWidth: getColumnWidth("Weight", "weight", participants)
        }
      ]}
      loading={loading}
      showPagination={false}
      getNoDataProps={() => {
        return { style: { background: "transparent", color: "transparent" } };
      }}
      getTdProps={(state, rowInfo, column, instance) => {
        return {
          style: {
            background: rowInfo
              ? rowInfo.index % 2 === 0 ? "#242423" : "#1A1919"
              : ""
          }
        };
      }}
    />
  );
};

const SessionsTable = props => {
  const { sessions, loading, onClick, editSession, getColumnWidth } = props;
  return (
    <ReactTable
      data={sessions}
      defaultPageSize={sessions.length}
      sorted={[
        {
          id: "start_datetime",
          asc: true
        }
      ]}
      columns={[
        {
          id: "start_datetime",
          Header: "From",
          accessor: row => {
            return dateformat(row.start_datetime, "dd mmm, HH:MM", true);
          },
          width: 150
        },
        {
          id: "end_datetime",
          Header: "To",
          accessor: row => {
            return dateformat(row.end_datetime, "dd mmm, HH:MM", true);
          },
          width: 150
        },
        {
          Header: "Max",
          accessor: "max_participants",
          minWidth: getColumnWidth("Max", "max_participants", sessions)
        },
        {
          Header: "Min",
          accessor: "min_participants",
          minWidth: getColumnWidth("Min", "min_participants", sessions)
        },
        {
          Header: "Price",
          accessor: "price",
          minWidth: getColumnWidth("Price", "price", sessions)
        },
        {
          id: "booked",
          Header: "Booked",
          accessor: row => {
            return row.booked + "/" + row.max_participants;
          },
          width: 80
        },
        {
          Header: "Guide",
          accessor: "guide",
          minWidth: getColumnWidth("Guide", "guide", sessions)
        },
        {
          Header: "",
          accessor: "editSession",
          Cell: ({ row }) => (
            <u>
              <a onClick={e => editSession(e, row)}>Show</a>
            </u>
          ),
          minWidth: 30
        }
      ]}
      loading={loading}
      showPagination={false}
      resizable
      getNoDataProps={() => {
        return { style: { background: "transparent", color: "transparent" } };
      }}
      getTrProps={(state, rowInfo, column, instance) => {
        if (
          rowInfo &&
          Object.values(rowInfo.original.participants).length > 0
        ) {
          return {
            onClick: e => onClick(e, rowInfo),
            style: {
              background: rowInfo
                ? rowInfo.index % 2 === 0 ? "#242423" : "#1A1919"
                : ""
            }
          };
        } else {
          return {
            style: {
              background: rowInfo
                ? rowInfo.index % 2 === 0 ? "#242423" : "#1A1919"
                : ""
            }
          };
        }
      }}
    />
  );
};

const ActivitiesTable = props => {
  const {
    activities,
    activityColumns,
    loading,
    onClick,
    getColumnWidth,
    editActivity
  } = props;
  return (
    <ReactTable
      data={activities}
      columns={[
        {
          Header: "",
          accessor: "color",
          width: 21,
          Cell: ({ row }) => <ColorSymbol color={row.color} />
        },
        {
          Header: "Activity Name",
          accessor: "name",
          minWidth: getColumnWidth("Activity Name", "name", activities)
        },
        {
          Header: "Level",
          accessor: "level",
          minWidth: getColumnWidth("Level", "level", activities)
        },
        {
          id: "booked",
          Header: "Booked",
          accessor: row => {
            return row.booked + "/" + row.max_booked;
          },
          minWidth: 80
        },
        {
          Header: "Sales",
          accessor: "sales",
          minWidth: getColumnWidth("Sales", "sales", activities)
        }
      ]}
      defaultPageSize={activities.length < 3 ? 3 : activities.length}
      showPagination={false}
      loading={loading}
      getNoDataProps={() => {
        return { style: { background: "transparent", color: "transparent" } };
      }}
      getTdProps={(state, rowInfo, column, instance) => {
        let props = {
          style: {
            background: rowInfo
              ? rowInfo.index % 2 === 0 ? "#242423" : "#1A1919"
              : ""
          }
        };
        if (column.id === "editActivity") {
          return props;
        } else {
          return {
            ...props,
            onClick: e => onClick(rowInfo)
          };
        }
      }}
    />
  );
};

class Sessions extends Component {
  constructor(props) {
    super(props);
    this.onSessionRowClick = this.onSessionRowClick.bind(this);
    this.onActivityRowClick = this.onActivityRowClick.bind(this);
    this.getColumnWidth = this.getColumnWidth.bind(this);
    this.editSession = this.editSession.bind(this);
  }

  editSession(e, row) {
    console.log(row._original);
    console.log(this.props.activityId);
    this.props.toggleSessionModal(this.props.activityId, row._original.id);
    this.props.setSession(null);
  }

  onSessionRowClick(e, state) {
    //e.target.name is to check if "show" button is clicked or the row.
    if (state && e.target.name === undefined) {
      const { id } = state.original;
      this.props.setSession(id);
    }
  }

  onActivityRowClick(state) {
    if (state) {
      const { id } = state.original;
      this.props.setActivity(id);
    }
  }

  getColumnWidth(Header, accessor, headers) {
    const values = Object.values(headers);
    let max = 0;
    const maxWidth = 300;
    const magicSpacing = 8;
    for (var i = 0; i < values.length; i++) {
      if (values[i] !== undefined && values[i][accessor] !== undefined) {
        if (JSON.stringify(values[i][accessor] || "null").length > max) {
          max = JSON.stringify(values[i][accessor] || "null").length;
        }
      }
    }
    return Math.min(maxWidth, Math.max(max, Header.length) * magicSpacing);
  }

  render() {
    const { sessionId, activityId } = this.props;
    if (!activityId) {
      const activities = Object.values(this.props.activities);
      return (
        <div className="aw-session-list-container width-100-percent">
          <ActivitiesTable
            activities={activities}
            getColumnWidth={this.getColumnWidth}
            editActivity={this.editActivity}
            loading={this.props.loading}
            onClick={this.onActivityRowClick}
          />
        </div>
      );
    } else {
      const sessions = Object.values(
        this.props.activities[activityId].sessions
      );
      if (
        !sessionId ||
        !this.props.activities[activityId].sessions[sessionId]
      ) {
        return (
          <div className="aw-session-list-container width-100-percent flex">
            <SessionsTable
              sessions={sessions}
              getColumnWidth={this.getColumnWidth}
              loading={this.props.loading}
              editSession={this.editSession}
              onClick={this.onSessionRowClick}
            />
            <div className="aw-session-list-footer">
              <a onClick={() => this.props.setActivity(null)}>
                <u style={{ color: "black" }}>Activities</u>
              </a>{" "}
              / {this.props.activities[activityId].name}
            </div>
          </div>
        );
      } else {
        const participants = Object.values(
          this.props.activities[activityId].sessions[sessionId].participants
        );
        return (
          <div className="aw-session-list-container width-100-percent flex">
            <ParticipantsTable
              participants={participants}
              loading={this.props.loading}
              getColumnWidth={this.getColumnWidth}
            />
            <div className="aw-session-list-footer">
              <a onClick={() => this.props.setActivity(null)}>
                <u style={{ color: "black" }}>Activities</u>
              </a>{" "}
              / {" "}
              <a onClick={() => this.props.setSession(null)}>
                <u style={{ color: "black" }}>
                  {this.props.activities[activityId].name}
                </u>
              </a>{" "}
              /{" "}
              {" " +
                dateformat(
                  new Date(
                    this.props.activities[activityId].sessions[sessionId]
                      .start_datetime
                  ),
                  "dd mmm, HH:MM",
                  true
                )}
            </div>
          </div>
        );
      }
    }
  }
}

export default Sessions;
