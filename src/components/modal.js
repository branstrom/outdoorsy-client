import React, { Component } from "react";

export default class Modal extends Component {
  render() {
    const { isActive } = this.props;
    const active = isActive ? "is-active" : "";
    return (
      <div className={`modal ${active}`}>
        {this.props.children}
      </div>
    );
  }
}
