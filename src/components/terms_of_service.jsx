import React from "react";

export const TermsOfService = () => {
  return (
    <div className="aw-terms-service">
      <p>
        By registering your account, you agree to our
        <a
          target="_blank"
          href="http://www.google.com"
          className="aw-terms-link"
        >
          Services Agreement
        </a>{" "}
        and the
        <a
          target="_blank"
          href="https://stripe.com/connect-account/legal"
          className="aw-terms-link"
        >
          Stripe Connected Account Agreement
        </a>.
      </p>
    </div>
  );
};
