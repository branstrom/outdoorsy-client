import React, { Component } from "react";
import { Field, reduxForm, formValueSelector } from "redux-form";
import { connect } from "react-redux";
import { Label } from "./form_fields/label";
import renderDatePicker from "./form_fields/date-picker";
import { RecurrentSession } from "./form_fields/recurrent_session";
import { FormInput } from "./form_fields/form_input";
import { DropDown } from "./form_fields/drop_down";
import { Button } from "./elements/button";
import moment from "moment";
import ButtonSwitcher from "./form_fields/button_switcher";
import validate from "../validations/create_session";
import LocationInput, {
  renderAutoComplete
} from "./form_fields/location_input";
import MeetingPoints from "./form_fields/meeting_points";
import Toggle from "react-toggle";
import ColoredScrollbars from "./colored_scrollbar";
import TimeInput from "time-input";

const selectableCurrencies = [{ text: "SEK", value: "SEK" }];
const renderTimePicker = ({ input: { onChange, value }, className }) => {
  return (
    <TimeInput
      onChange={onChange}
      value={value || "00:00"}
      className={className}
      defaultValue="12:00"
      type="time"
    />
  );
};

class CreateSession extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recurrent: false,
      activity: false,
      meeting: false,
      additional: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.cancel = this.cancel.bind(this);
    this.delete = this.delete.bind(this);
  }
  componentWillMount() {
    if (this.props.guides.length === 0) {
      this.props.getGuides(this.props.organizationId);
    }
  }
  componentWillReceiveProps(nextProps) {
    if (
      (!this.props.activity_id && nextProps.activity_id) ||
      this.props.activity_id !== nextProps.activity_id
    ) {
      if (nextProps.activity_id) {
        this.props.getActivityLocations(nextProps.activity_id);
      }
    }
    if (!this.props.modalSessionId && nextProps.modalSessionId) {
      this.props.fetchSession(nextProps.modalSessionId);
    }
    if (
      this.props.modalSessionId !== undefined &&
      this.props.modalSessionId !== nextProps.modalSessionId
    ) {
      this.props.reset("CreateSessionForm");
    }
  }

  delete() {
    console.log("DELETING", this.props);
    const { modalSessionId } = this.props;
    if (modalSessionId) {
      // eslint-disable-next-line no-restricted-globals
      if (confirm("Are you sure you want to delete this session?")) {
        this.props.removeSession(modalSessionId);
      }
    }
  }

  cancel() {
    this.props.clearEditSession();
    this.props.reset("CreateSessionForm");
    this.props.toggleSessionModal(null, null);
  }

  handleSubmit(values) {
    console.log("Submitting form, values:", values);
    console.log(this.props);
    this.props.createSession(values, this.props.activity_id);
    this.props.clearEditSession();
    //this.props.reset("CreateSessionForm");
  }

  render() {
    const { handleSubmit } = this.props;
    const activity = this.props.activities.find(activity => {
      if (this.props.activity_id === activity.id) {
        return activity;
      }
    });
    return (
      <div>
        <div className="modal-background" />
        <div className="modal-card">
          <header className="modal-card-head">
            <div className="modal-card">
              <h3>
                {activity ? this.props.modalSessionId ? (
                  "Edit session - " + activity.activity_name
                ) : (
                  "Add session - " + activity.activity_name
                ) : (
                  ""
                )}
              </h3>
            </div>
          </header>
          <ColoredScrollbars style={{ height: 4000 }} color={`#469CDD`}>
            <section className="modal-card-body">
              <form onSubmit={handleSubmit(this.handleSubmit)}>
                {activity ? activity.location : ""}
                <div className="aw-activity-input">
                  <Label label="Date" content="1" />
                  <div className="aw-session-date">
                    <div className="columns" style={{ marginBottom: "-20px" }}>
                      <div className="column is-1">Start</div>
                      <div className="column is-2">
                        <Field name="start_date" component={renderDatePicker} />
                      </div>
                      <div className="column is-2">
                        <Field
                          name="start_time"
                          component={renderTimePicker}
                          className="input aw-input"
                        />
                      </div>
                      <div className="column is-3" />
                    </div>
                    <div className="columns">
                      <div className="column is-1">End</div>
                      <div className="column is-2">
                        <Field name="end_date" component={renderDatePicker} />
                      </div>
                      <div className="column is-2">
                        <Field
                          name="end_time"
                          component={renderTimePicker}
                          className="input aw-input"
                        />
                      </div>
                      <div className="column is-3" />
                    </div>
                  </div>
                </div>
                <div className="aw-activity-input">
                  <Label label="Price" content="2" />
                  <div className="aw-flex-row">
                    <div>
                      <b>cost per person</b>
                    </div>
                    <Field
                      name="amount"
                      component={FormInput}
                      type="number"
                      style="no-spin aw-input"
                    />
                    <div>
                      <b>Currency</b>
                    </div>
                    <Field
                      name="currency"
                      component={DropDown}
                      data={selectableCurrencies}
                    />
                  </div>
                </div>
                <div className="aw-activity-input">
                  <Label label="Participants" content="3" />
                  <div className="aw-flex-row">
                    <div>
                      <b>Min</b>
                    </div>
                    <Field
                      name="min_participants"
                      component={FormInput}
                      type="number"
                      style="no-spin aw-input aw-input-short"
                    />
                    <div>
                      <b>Max</b>
                    </div>
                    <Field
                      name="max_participants"
                      component={FormInput}
                      type="number"
                      style="no-spin aw-input aw-input-short"
                    />
                  </div>
                </div>
                <div className="aw-activity-input">
                  <Label label="Location" content="4" />
                  <div className="aw-flex-row">
                    <Field
                      name="activity_point_id"
                      component={DropDown}
                      placeholder="Choose location"
                      data={this.props.activityPoints}
                    />
                  </div>
                </div>
                <div className="aw-activity-input">
                  <Label label="Pickup spot" content="5" />
                  <div className="aw-flex-row">
                    <MeetingPoints
                      name="locations"
                      data={this.props.meetingPoints}
                      time_of_day={this.props.time_of_day}
                      textName="meetins_point"
                    />
                  </div>
                </div>
                <div className="aw-activity-input">
                  <Label label="Extra info" content="8" />
                  <div className="aw-flex-row">
                    <Field
                      name="additional_info"
                      component={FormInput}
                      style="aw-additional-info"
                      rows={3}
                      type="text"
                    />
                  </div>
                </div>
                <div className="aw-activity-input">
                  <Label label="Guide" content="9" />
                  <div className="aw-flex-row">
                    <Field
                      name="guide_id"
                      component={DropDown}
                      data={this.props.guides}
                    />
                  </div>
                </div>
                <div className="aw-activity-buttons-container">
                  {this.props.modalSessionId &&
                  this.props.showDelete() && (
                    <Button
                      className="margin-right-sm"
                      text="Delete"
                      type="button"
                      loading={this.props.removing}
                      onClick={this.delete}
                    />
                  )}
                  {!this.props.modalSessionId && (
                    <Button
                      type="submit"
                      className="aw-session-save margin-right-sm"
                      text="Save Session"
                      loading={this.props.loading}
                      disabled={this.props.pristine || this.props.invalid}
                    />
                  )}
                  {this.props.modalSessionId && (
                    <Button
                      type="submit"
                      className="aw-activity-save"
                      text="Duplicate Session"
                      loading={this.props.loading}
                      disabled={this.props.pristine || this.props.invalid}
                    />
                  )}
                </div>
              </form>
            </section>
          </ColoredScrollbars>
        </div>
      </div>
    );
  }
}

CreateSession = reduxForm({
  validate,
  form: "CreateSessionForm",
  enableReinitialize: true
})(CreateSession);
const selector = formValueSelector("CreateSessionForm");
CreateSession = connect(state => {
  // pull initial values from account reducer
  const start_time = selector(state, "start_time");
  const end_time = selector(state, "end_time");
  const start_date = selector(state, "start_date");
  const end_date = selector(state, "end_date");
  const time_of_day = selector(state, "time_of_day");
  return {
    start_time,
    end_time,
    start_date,
    end_date,
    time_of_day,
    initialValues: state.editSession
  };
})(CreateSession);

export default CreateSession;
