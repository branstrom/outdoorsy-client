import React from "react";

const ColorSymbol = ({ color }) => {
  console.log("COLOR: ", color);
  return (
    <div className="aw-color-symbol">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 78.3 171.5"
        fill={color}
        width="10px"
      >
        <defs />
        <title>Resurs 2</title>
        <g id="Lager_2" data-name="Lager 2">
          <g id="Lager_1-2" data-name="Lager 1">
            <polyline
              fill={color}
              points="66.8 82.7 78.3 82.7 43.7 0 34.6 0 0 82.7 11.4 82.7"
            />
            <polygon
              fill={color}
              points="43.6 171.5 78.3 88.8 66.8 88.8 39.6 159.6 11.4 88.8 0 88.8 34.5 171.5 43.6 171.5"
            />
          </g>
        </g>
      </svg>
    </div>
  );
};

export default ColorSymbol;
