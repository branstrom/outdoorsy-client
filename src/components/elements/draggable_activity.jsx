import React, { Component } from "react";
import { DragSource } from "react-dnd";
import ColorSymbol from "./color_symbol";

class DraggableActivity extends Component {
  render() {
    const { isDragging, connectDragSource } = this.props;
    const { name, id, color } = this.props;

    return connectDragSource(
      <div className="aw-activity-list-item">
        <div className="margin-right-sm">
          <ColorSymbol color={color} />
        </div>
        <div>
          {name}
        </div>
      </div>
    );
  }
}

const type = "activity";
const spec = {
  beginDrag(props) {
    return {
      name: props.name,
      id: props.id
    };
  },

  endDrag(props, monitor) {
    const item = monitor.getItem();
    const dropResult = monitor.getDropResult();

    if (dropResult) {
      props.toggleSessionModal(item.id);
    }
  }
};

const collect = (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
});

export default DragSource(type, spec, collect)(DraggableActivity);
