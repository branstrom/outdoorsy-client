import React from "react";

export const Button = props => {
  const { loading, text, className, type, onClick, disabled, cancel } = props;
  let style = className;
  if (loading) {
    style += " is-loading";
  }
  return (
    <button
      className={`button aw-modal-button ${style}`}
      type={type}
      onClick={onClick}
      disabled={false}
    >
      {text}
    </button>
  );
};
