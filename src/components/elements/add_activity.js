import React from "react";

const AddActivity = props => {
  return (
    <div
      className="flex-container fa-button margin-bottom"
      onClick={props.onClick}
    >
      <i
        className="fa fa-plus-circle fa-2x margin-right-sm"
        aria-hidden="true"
      />
      Add Activity
    </div>
  );
};

export default AddActivity;
