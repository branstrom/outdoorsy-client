import React from 'react';

export default (props) => {
    return (
        <div>
            <h3>Activity details</h3>
            Activity #{parseInt(props.match.params.activityId, 10)}
        </div>
    )
}