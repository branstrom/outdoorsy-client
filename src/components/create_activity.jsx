import React, { Component } from "react";
import {
  Field,
  reduxForm,
  formValueSelector,
  FieldArray,
  SubmissionError
} from "redux-form";
import { connect } from "react-redux";
import { animateScroll, Element } from "react-scroll";
import { Label } from "./form_fields/label";
import ButtonSwitcher from "./form_fields/button_switcher";
import ItemsList from "./form_fields/items_list";
import ImageUploader from "./form_fields/image_uploader";
import { FormInput } from "./form_fields/form_input";
import { TextArea } from "./form_fields/text_area";
import { DropDown } from "./form_fields/drop_down";
import LocationInput from "./form_fields/location_input";
import ColorPicker from "./form_fields/color_picker";
import { Button } from "./elements/button";
import { renderItems } from "./form_fields/items_list";
import getActivityTypeId from "../utils/getActivityTypeId";
import validate from "../validations/create_activity";
import ColoredScrollbars from "./colored_scrollbar";
import { CREATE_ACTIVITY_ERRORS } from "../actions/types";

const selectableLevels = [
  { text: "Beginner", value: "BEGINNER" },
  { text: "Intermediate", value: "INTERMEDIATE" },
  { text: "Advanced", value: "ADVANCED" }
];

const selectableColors = [
  { text: "Red", value: "RED" },
  { text: "Yellow", value: "YELLOW" },
  { text: "Blue", value: "BLUE" }
];

const displayGlobal = (errors, dispatch) => {
  console.log(errors);
  console.log(dispatch);
  dispatch({ type: CREATE_ACTIVITY_ERRORS, payload: errors });
};

class CreateActivity extends Component {
  constructor(props) {
    super(props);
    this.cancel = this.cancel.bind(this);
    this.delete = this.delete.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.state = {
      errors: null
    };
    this.showPreview = this.showPreview.bind(this);
    this.selectableTypes = this.selectableTypes.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.modalActivityId && nextProps.modalActivityId) {
      this.props.fetchActivity(nextProps.modalActivityId);
    }
    if (this.props.removing && !nextProps.removing) {
      this.props.reset();
      this.props.toggleActivityModal();
    }
  }

  showPreview() {
    let host = window.location.hostname;
    if (host === "localhost") {
      host = "http://localhost:3002";
    } else {
      host = "https://" + host;
    }
    const url = `${host}/booking/${this.props.modalActivityId}?preview`;
    var tab = window.open(url, "_blank");
    if (tab) {
      //Browser has allowed it to be opened
      tab.focus();
    } else {
      //Browser has blocked it
      alert("Please allow popups for this website");
    }
    //Open a window with the correct url with Preiew URL-thing
  }

  cancel() {
    console.log("CANCEL")
    this.props.reset();
    this.props.toggleActivityModal();
  }
  handleSubmit(values, event) {
    console.log(event);
    console.log("Submitting form, values:", values);
    if (this.props.modalActivityId) {
      this.handleUpdate(values);
    } else {
      values.activity_type_id = getActivityTypeId(
        values.activity_type,
        this.props.types
      );
      console.log("Submitting form, values:", values);
      this.props.createActivity(values);
    }
  }

  handleUpdate(values) {
    const { oldValues } = this.props;
    values.activity_type_id = getActivityTypeId(
      values.activity_type,
      this.props.types
    );
    this.props.updateActivity(this.props.modalActivityId, values, oldValues);
  }

  delete() {
    const { modalActivityId } = this.props;
    if (modalActivityId) {
      // eslint-disable-next-line no-restricted-globals
      if (confirm("Are you sure you want to delete this activity?")) {
        this.props.removeActivity(modalActivityId);
      }
    }
  }
  selectableTypes() {
    if (this.props.types.length > 0) {
      return this.props.types.filter(type => {
        return type.category === this.props.activity_category;
      });
    } else {
      return [];
    }
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div>
        <div className="modal-background" />
        <div className="modal-card">
          <header className="modal-card-head">
            <div className="modal-card">
              <h3>
                {this.props.modalActivityId ? "Edit Activity" : "New Activity"}
              </h3>
            </div>
          </header>
          <ColoredScrollbars
            style={{ height: 4000 }}
            color={`rgb(217, 38, 129)`}
          >
            <section className="modal-card-body">
              <form onSubmit={handleSubmit(this.handleSubmit)}>
                <div>
                  <div className="aw-activity-input">
                    <Label label="Activity name" content="1" />
                    <Field
                      name="activity_name"
                      component={FormInput}
                      placeholder="Type you activity name here"
                      style="aw-input"
                    />
                    <div
                      className="aw-activity-input margin-left"
                      style={{ marginBottom: 0 }}
                    >
                      <Label label="Color" style="margin-right" />
                      <Field name="color" component={ColorPicker} />
                    </div>
                  </div>
                  <div className="aw-activity-input">
                    <Label label="Category" content="2" />
                    <Field
                      name="activity_category"
                      component={DropDown}
                      data={this.props.categories}
                      style="aw-input"
                    />
                  </div>
                  <div className="aw-activity-input">
                    <Label label="Type of activity" content="3" />
                    <Field
                      name="activity_type"
                      component={DropDown}
                      data={this.selectableTypes()}
                      style="aw-input"
                    />
                  </div>
                  <div className="aw-activity-input">
                    <Label label="Level" content="4" />
                    <Field
                      name="level"
                      component={DropDown}
                      data={selectableLevels}
                      style="aw-input"
                    />
                  </div>
                  <div className="aw-activity-input">
                    <Label label="Cover image" content="5" />
                    <ImageUploader />
                  </div>
                  <div className="aw-activity-input">
                    <Label label="Short Description" content="6" />
                    <Field
                      name="description"
                      component={TextArea}
                      placeholder="Describe your activity in 200 characters..."
                      rows="4"
                      maxCharacters={200}
                    />
                  </div>
                  <div className="aw-activity-input">
                    <Label label="Description" content="7" />
                    <Field
                      name="detailed_description"
                      maxCharacters={3000}
                      placeholder="Describe your activity in 3000 characters..."
                      component={TextArea}
                      rows="1"
                    />
                  </div>
                  <div className="aw-activity-input">
                    <Label label="Location" content="8" />
                    <LocationInput
                      name="activity_point_description"
                      buttonText="Add"
                      textName="activity_point_description_text"
                      showDelete={
                        this.props.showDelete() || !this.props.modalActivityId
                      }
                      text={this.props.activity_point_description_text}
                    />
                  </div>
                  <div className="aw-activity-input">
                    <Label label="Pickup spots" content="9" />
                    <LocationInput
                      name="meeting_point_description"
                      buttonText="Add"
                      showDelete={
                        this.props.showDelete() || !this.props.modalActivityId
                      }
                      textName="meeting_point_description_text"
                      text={this.props.meeting_point_description_text}
                    />
                  </div>
                  <div className="aw-activity-input">
                    <Label label="Included" content="10" />
                    <FieldArray
                      name="included"
                      textName="includedText"
                      component={renderItems}
                      text={this.props.includedText}
                    />
                  </div>
                  <div className="aw-activity-input">
                    <Label label="Required" content="11" />
                    <FieldArray
                      name="required"
                      textName="requiredText"
                      component={renderItems}
                      text={this.props.requiredText}
                    />
                  </div>
                </div>
                <div className="aw-activity-buttons-container">
                  {this.props.modalActivityId && (
                    <Button
                      className="margin-right-sm"
                      text="Preview"
                      type="button"
                      loading={this.props.updating}
                      onClick={this.showPreview}
                    />
                  )}
                  {this.props.modalActivityId &&
                    this.props.showDelete() && (
                      <Button
                        className="margin-right-sm"
                        text="Delete"
                        type="button"
                        loading={this.props.removing}
                        onClick={this.delete}
                      />
                    )}

                  <Button
                    type="submit"
                    className="aw-activity-save"
                    text="Save Activity"
                    loading={this.props.loading || this.props.updating}
                    disabled={this.props.pristine || this.props.invalid}
                  />
                </div>
                <div style={{ textAlign: "right" }}>
                  {this.props.errors && "Please make sure there is no errors"}
                </div>
              </form>
            </section>
          </ColoredScrollbars>
        </div>
        <button
          className="modal-close is-large"
          onClick={this.cancel}
        />
      </div>
    );
  }
}

/**
 * Connecta först med Redux, sen reduxForm
 */

CreateActivity = reduxForm({
  validate,
  form: "CreateActivityForm",
  enableReinitialize: true,
  onSubmitFail: (errors, dispatch) => displayGlobal(errors, dispatch)
})(CreateActivity);
const selector = formValueSelector("CreateActivityForm");
CreateActivity = connect(state => {
  const includedText = selector(state, "includedText");
  const requiredText = selector(state, "requiredText");
  const activity_point_description_text = selector(
    state,
    "activity_point_description_text"
  );
  const meeting_point_description_text = selector(
    state,
    "meeting_point_description_text"
  );
  const activity_category = selector(state, "activity_category");
  return {
    includedText,
    requiredText,
    meeting_point_description_text,
    activity_point_description_text,
    activity_category,
    initialValues: state.editActivity
  };
})(CreateActivity);

export default CreateActivity;
