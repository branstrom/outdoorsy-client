import React, { Component } from "react";
import ValidationError from "./validation_error";

export default class ButtonSwitcher extends Component {
  onClick(value) {
    this.props.input.onChange(value);
  }

  render() {
    const {
      input: { value },
      data,
      style,
      meta: { touched, error, warning }
    } = this.props;
    return (
      <div className="field">
        <div className="field has-addons">
          {data.map((button, index) =>
            <p className="control" key={index}>
              <a
                onClick={this.onClick.bind(this, button.value)}
                className={
                  value === button.value ? "is-active button" : "button"
                }
              >
                {button.text}
              </a>
            </p>
          )}
        </div>
        <ValidationError touched={touched} error={error} warning={warning} />
      </div>
    );
  }
}
