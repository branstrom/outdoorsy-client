import React, { Component, PropTypes } from "react";
import Dropzone from "react-dropzone";
import ValidationError from "./validation_error";
import plus from "../../img/plus-white-small.svg";
import { uploadImages } from "../../actions/activities_actions";

export default class ImageUpload extends React.Component {
  handleDropOrClick = (acceptedFiles, rejectedFiles, e) => {
    let eventOrValue = e;
    let { input: { onChange, onBlur } } = this.props;
    if (e.type === "drop" || e.type === "change") {
      if (acceptedFiles.length) {
        // FileList or [File]
        eventOrValue = acceptedFiles[0];
      } else {
        eventOrValue = null;
      }
    }
    onBlur(eventOrValue); // update touched
    onChange(eventOrValue); // update value
  };

  render() {
    const {
      name,
      meta: { touched, error, warning },
      large,
      input: { value, onChange }
    } = this.props;
    let className = "photo-upload-box";
    let text = "";
    let size = "20px";
    if (large) {
      className = "photo-upload-box-large";
      text = "Cover photo or video";
      size = "30px";
    }
    let dropzoneProps = {
      onDrop: this.handleDropOrClick,
      className,
      accept: "image/jpeg, image/png, image.jpg, video/mp4"
    };
    return (
      <div>
        <Dropzone {...dropzoneProps}>
          {!value && <img src={plus} width={size} height={size} />}
          {value && <img src={value.preview} className="image-upload" />}
        </Dropzone>
        <div className="photo-upload-box-text">{text}</div>
        <ValidationError touched={touched} error={error} warning={warning} />
      </div>
    );
  }
}
