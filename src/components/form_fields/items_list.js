import React, { Component } from "react";
import { Field, FieldArray } from "redux-form";
import ValidationError from "./validation_error";

export const renderItems = ({
  fields,
  textName,
  text,
  meta: { submitFailed, error, warning },
  meta
}) => (
  <div className="aw-item-outer">
    <div className="aw-flex-row">
      <Field
        className="input aw-input"
        type="text"
        name={textName}
        component="input"
      />
      <a
        className="aw-modal-button"
        onClick={() => {
          if (text && text.length > 0) {
            fields.push({ name: text });
          }
        }}
      >
        Add
      </a>
    </div>

    <div className="tags">
      {fields.map((item, index) => (
        <span className="tag" key={index}>
          <div className="aw-item">{fields.get(index).name}</div>

          <button
            key={index}
            className="delete is-small"
            onClick={event => {
              event.preventDefault();
              fields.remove(index);
            }}
          />
        </span>
      ))}
    </div>

    <ValidationError touched={submitFailed} error={error} warning={warning} />
  </div>
);

export default class ItemsList extends Component {
  render() {
    const { name, textName, text } = this.props;
    console.log(this.props);
    return (
      <div className="field">
        <FieldArray
          name={name}
          component={renderItems}
          textName={textName}
          text={text}
        />
      </div>
    );
  }
}
