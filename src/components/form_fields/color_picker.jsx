import React, { Component } from "react";
import { Field } from "redux-form";
import ValidationError from "./validation_error";
import ColorSymbol from "../elements/color_symbol";
const colors = [
  "#D42D7E",
  "#F25F5C",
  "#469CDD",
  "#63CCCA",
  "#FBEE4F",
  "#BDD9BF",
  "#8AC926",
  "#898952",
  "#B2945B",
  "#440381"
];

export default class ColorPicker extends Component {
  render() {
    const {
      input: { value },
      style,
      meta: { touched, error, warning }
    } = this.props;
    const colorsComponents = colors.map(color => {
      return <ColorSymbol color={color} />;
    });
    return (
      <div className="field">
        <div className="field has-addons">
          {colorsComponents.map((color, index) => (
            <div className="control" key={index}>
              {
                <a
                  onClick={() => this.props.input.onChange(color.props.color)}
                  style={{
                    marginRight: "10px",
                    borderRadius: "3px",
                    borderRadius: "100px"
                  }}
                  className={
                    value === color.props.color ? "is-active button" : "button"
                  }
                >
                  {color}
                </a>
              }
            </div>
          ))}
        </div>
        <ValidationError touched={touched} error={error} warning={warning} />
      </div>
    );
  }
}
