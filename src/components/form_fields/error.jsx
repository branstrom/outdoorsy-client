import React from "react";

export const Error = props => {
  const message = props.message;
  return (
    <div>
      {message}
    </div>
  );
};
