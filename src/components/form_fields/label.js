import React from "react";

export const Label = props => {
  const { label, content, style } = props;
  let className = "label";
  if (content) {
    className += " aw-numbered-label";
  }
  if (style) {
    className += ` ${style}`;
  }
  return (
    <label className={className} content={content}>
      {label}
    </label>
  );
};
