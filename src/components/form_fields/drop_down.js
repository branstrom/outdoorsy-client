import React from "react";
import { Field } from "redux-form";
import ValidationError from "./validation_error";

export const DropDown = props => {
  const {
    input: { name },
    data,
    meta: { touched, error, warning },
    style,
    placeholder
  } = props;
  let className = "";
  if (style) {
    className += ` ${style}`;
  }
  return (
    <div className="field aw-flex-column">
      <div>
        <span className="select">
          <Field className={className} name={name} component="select">
            <option>{placeholder}</option>
            {data.map((row, index) => {
              return (
                <option value={row.value} key={index}>
                  {row.text}
                </option>
              );
            })}
          </Field>
        </span>
      </div>
      <ValidationError touched={touched} error={error} warning={warning} />
    </div>
  );
};
