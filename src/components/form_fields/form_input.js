import React from "react";
import { Field } from "redux-form";
import ValidationError from "./validation_error";

const renderField = ({
  input,
  type,
  className,
  placeholder,
  rows,
  autoComplete,
  meta: { touched, error, warning }
}) => (
  <div className="aw-flex-column">
    <div>
      <input
        {...input}
        type={type}
        rows
        className={className}
        placeholder={placeholder}
        autoComplete={autoComplete || "on"}
      />
    </div>
    <ValidationError touched={touched} error={error} warning={warning} />
  </div>
);

export const FormInput = props => {
  const {
    input: { name },
    type,
    placeholder,
    style,
    rows,
    autoComplete
  } = props;
  let className = "input aw-input";
  if (style) {
    className += ` ${style}`;
  }
  return (
    <div className="field">
      <Field
        className={className}
        rows={rows || 1}
        type={type || "text"}
        name={name}
        autoComplete={autoComplete}
        placeholder={placeholder}
        component={renderField}
      />
    </div>
  );
};
