import React, { Component } from "react";
import { Field, FieldArray } from "redux-form";
import PlacesAutocomplete from "react-places-autocomplete";
import ValidationError from "./validation_error";

const options = {
  location: window.google ? new window.google.maps.LatLng(59, 17.8) : "",
  radius: 100000
};
const style = {
  autocompleteContainer: {
    position: "relative",
    zIndex: 100,
    width: "300px",
    border: 0
  },
  input: {
    fontFamily: "Raleway, sans-serif",
    fontSize: "12pt",
    outline: 0,
    borderRadius: "5px",
    width: "300px",
    borderWidth: 0,
    border: 0,
    boxShadow: 0
  },
  autocompleteItemActive: {
    backgroundColor: "#ffffff"
  }
};
export const renderAutoComplete = ({ input: { value, onChange } }) => (
  <div className="field">
    {console.log(value)}
    <PlacesAutocomplete
      inputProps={{
        value: value,
        onChange: onChange
      }}
      styles={style}
      options={options}
    />
  </div>
);

const renderLocationInput = ({
  fields,
  textName,
  buttonText,
  text,
  showDelete,
  meta: { submitFailed, error, warning }
}) => (
  <div>
    <div className="aw-flex-row">
      <Field name={textName} component={renderAutoComplete} />
      <a
        className="aw-modal-button"
        onClick={() => {
          if (text && text.length > 0) {
            fields.push({ directions: text });
          }
        }}
      >
        {buttonText}
      </a>
    </div>
    {fields.length > 0 && (
      <div className="aw-location-container">
        <ul>
          {fields.map((item, index) => (
            <li key={index}>
              <div className="columns">
                <div className="column is-10">
                  {fields.get(index).directions}
                </div>
                <div className="column aw-flex-right">
                  {showDelete && (
                    <button
                      key={index}
                      className="delete is-small"
                      onClick={event => {
                        event.preventDefault();
                        fields.remove(index);
                      }}
                    />
                  )}
                </div>
              </div>
            </li>
          ))}
        </ul>
      </div>
    )}
    <ValidationError touched={submitFailed} error={error} warning={warning} />
  </div>
);

export default class LocationInput extends Component {
  render() {
    return (
      <div className="field">
        <FieldArray {...this.props} component={renderLocationInput} />
      </div>
    );
  }
}
