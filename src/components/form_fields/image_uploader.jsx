import React from "react";
import { Field } from "redux-form";
import ImageUpload from "./image_upload";

const ImageUploader = () => {
  return (
    <div className="aw-activity-container">
      <div className="columns">
        <div className="column is-8">
          <Field name="image_upload_0" component={ImageUpload} large />
        </div>
        <div className="column">
          <div className="columns">
            <div className="column">
              <Field name="image_upload_1" component={ImageUpload} />
            </div>
            <div className="column">
              <Field name="image_upload_2" component={ImageUpload} />
            </div>
          </div>
          <div className="columns">
            <div className="column">
              <Field name="image_upload_3" component={ImageUpload} />
            </div>
            <div className="column">
              <Field name="image_upload_4" component={ImageUpload} />
            </div>
          </div>
          <div className="columns">
            <div className="column">
              <Field name="image_upload_5" component={ImageUpload} />
            </div>
            <div className="column">
              <Field name="image_upload_6" component={ImageUpload} />
            </div>
          </div>
          Images
        </div>
      </div>
    </div>
  );
};

export default ImageUploader;
