import React, { Component } from "react";

const ValidationError = ({ touched, error, warning }) => {
  if (touched) {
    if (error) {
      return (
        <div
          style={{
            position: "relative",
            color: "rgb(242, 133, 133)",
            maxWidth: "500px"
          }}
        >
          <div style={{ position: "relative", fontSize: "10pt" }}>
            <b>{error}</b>
          </div>
        </div>
      );
    }
    if (warning) {
      return <span>{warning}</span>;
    } else {
      return <div />;
    }
  } else {
    return <div />;
  }
};

export default ValidationError;
