import React, { Component } from 'react';

class PhotoUpload extends Component {

    constructor(props) {
        super(props);
        this.state = { file: [] };
    }

    _onChange() {
        // Assuming only image
        const file = this.refs.file.files[ 0 ];
        let reader = new FileReader();
        const url = reader.readAsDataURL(file);

        reader.onloadend = function (e) {
            this.setState({
                imgSrc: [ reader.result ]
            })
        }.bind(this);
        console.log(url) // Would see a path?
        // TODO: concat files
    }

    render() {
        return (
            <div>
                <input
                    ref="file"
                    type="file"
                    name="user[image]"
                    multiple="true"
                    onChange={this._onChange.bind(this)}/>
                {/* Only show first image, for now. */}
                <img src={this.state.imgSrc} style={{width: '300px', height: '300px'}}/>
            </div>
        )
    }
}


export default PhotoUpload;
