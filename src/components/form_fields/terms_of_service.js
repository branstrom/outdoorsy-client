import React from "react";
import { Field } from "redux-form";
import ValidationError from "./validation_error";

export const Terms = ({ meta: { touched, error, warning } }) => {
  return (
    <div>
      <p>
        <b>General Terms of Services</b>
      </p>
      <div className="columns is-gapless is-marginless	">
        <div className="column is-1 has-text-centered">
          <Field name="terms" id="terms" component="input" type="checkbox" />
        </div>
        <div className="column" style={{ fontSize: "14px", marginTop: "3px" }}>
          By clicking the checkbox you agree to our{" "}
          <a
            target="_blank"
            href="/General-Terms-of-Services.pdf"
          >
            <u>General Terms Of Services</u>
          </a>, which includes Stripe Connected Account Agreement and our
          Privacy Policy in your relation with Outdoorsy and STRIPE.
        </div>
      </div>
      <ValidationError touched={touched} error={error} warning={warning} />
    </div>
  );
};
