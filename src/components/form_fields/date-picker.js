import DatePicker from "react-datepicker";
import moment from "moment";
import React from "react";
import ValidationError from "./validation_error";
import "react-datepicker/dist/react-datepicker.css";
import "moment/locale/en-gb";

class renderDatePicker extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(date) {
    this.props.input.onChange(moment(date).format("YYYY-MM-DD"));
  }

  render() {
    const {
      input,
      placeholder,
      defaultValue,
      name,
      meta: { touched, error, warning }
    } = this.props;
    return (
      <div>
        {console.log(input.value)}
        <DatePicker
          name={name}
          dateFormat="YYYY-MM-DD"
          selected={input.value ? moment(input.value, "YYYY-MM-DD") : null}
          onChange={this.handleChange}
          className="input aw-input aw-input-date"
          placeholderText="Select date"
          locale="en-gb"
          minDate={moment("YYYY-MM-DD")}
        />
        <ValidationError touched={touched} error={error} warning={warning} />
      </div>
    );
  }
}

export default renderDatePicker;
