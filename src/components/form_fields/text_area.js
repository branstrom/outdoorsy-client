import React from "react";
import { Field } from "redux-form";
import ValidationError from "./validation_error";

import RichTextMarkdown from "./rich_text_markdown";

import count from "../../utils/countTextLength";

const renderField = ({
  input,
  input: { value, onChange },
  type,
  className,
  placeholder,
  rows,
  maxCharacters,
  meta: { touched, error, warning }
}) =>
  <div className="aw-flex-column">
    <div className="aw-text-area-outer">
      <RichTextMarkdown
        input={input}
        value={value}
        onChange={onChange}
        className={className}
        placeholder={placeholder}
      />
      <div className="aw-text-area-count">
        {maxCharacters - count(value) < 0
          ? error
          : maxCharacters - count(value) + " characters left"}
      </div>
    </div>
    <ValidationError touched={touched} error={error} warning={warning} />
  </div>;

export const TextArea = props => {
  const {
    input: { name },
    type,
    placeholder,
    style,
    rows,
    maxCharacters
  } = props;
  let className = "";
  if (style) {
    className += ` ${style}`;
  }
  return (
    <div className="field">
      <Field
        className={className}
        name={name}
        placeholder={placeholder}
        component={renderField}
        rows={rows}
        maxCharacters={maxCharacters}
        validate={value =>
          count(value) > maxCharacters
            ? `To many characters (${count(value) - maxCharacters})`
            : undefined}
      />
    </div>
  );
};
