import React, { Component } from "react";
import { Field, FieldArray } from "redux-form";
import { DropDown } from "./drop_down";
import { Button } from "../elements/button";
import { renderAutoComplete } from "./location_input";
import ValidationError from "./validation_error";

class renderItems extends Component {
  constructor(props) {
    super(props);
    this.state = { value: "Choose meeting point" };
    this.handleChange = this.handleChange.bind(this);
    this.addMeeting = this.addMeeting.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  addMeeting(event) {
    const { fields, data, time_of_day } = this.props;
    if (this.state.value === "Choose meeting point") {
    } else if (this.state.value === "Add new meeting point") {
      //fields.push({ text: "", value: null, time_of_day });
    } else {
      const selected = data.find(item => {
        return item.value == this.state.value;
      });
      let exists = false;
      fields.forEach((field, index) => {
        if (
          selected &&
          fields.get(index) &&
          fields.get(index).value === selected.value
        ) {
          exists = true;
        }
      });
      if (!exists && time_of_day) {
        fields.push({
          text: selected.text,
          value: selected.value,
          time_of_day
        });
      }
      event.preventDefault();
    }
  }
  render() {
    const {
      fields,
      textName,
      text,
      data,
      meta: { submitFailed, error, warning }
    } = this.props;
    return (
      <div>
        <div className="columns">
          <div className="column is-5">
            <div className="select">
              <select value={this.state.value} onChange={this.handleChange}>
                <option>Choose meeting point</option>
                {data.map((row, index) => {
                  return (
                    <option value={row.value} key={index}>
                      {row.text}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>
          <div className="column is-3">
            <Field
              name="time_of_day"
              component="input"
              type="time"
              className="input aw-input aw-input-short"
            />
          </div>
          <div className="column is-3">
            <Button
              type="button"
              text="Confirm"
              loading={false}
              onClick={this.addMeeting}
            />
          </div>
        </div>
        {fields.length > 0 && (
          <div className="aw-session-location-container">
            <ul>
              {fields.map((field, index) => {
                return (
                  <li key={index}>
                    <div className="columns">
                      <div className="column">
                        {fields.get(index).text !== "" ? (
                          fields.get(index).text
                        ) : (
                          <Field
                            name={`${field}.text`}
                            component={renderAutoComplete}
                          />
                        )}
                      </div>
                      <div className="column is-2">
                        {fields.get(index).time_of_day}
                      </div>
                      <div className="column aw-flex-right is-2">
                        <button
                          key={index}
                          className="delete is-small"
                          onClick={() => fields.remove(index)}
                        />
                      </div>
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>
        )}
        <ValidationError touched={submitFailed} error={error} />
      </div>
    );
  }
}

export default class MeetingPoints extends Component {
  render() {
    const { name, textName, text, data, time_of_day } = this.props;
    return (
      <div className="field">
        <FieldArray
          name={name}
          component={renderItems}
          textName={textName}
          text={text}
          time_of_day={time_of_day}
          data={data}
        />
      </div>
    );
  }
}
