import React, { Component, PropTypes } from "react";
import RichTextEditor from "react-rte";
const toolbarConfig = {
  display: ["INLINE_STYLE_BUTTONS"],
  INLINE_STYLE_BUTTONS: [
    { label: "Bold", style: "BOLD", className: "aw-text-area-buttons" },
    { label: "Italic", style: "ITALIC", className: "aw-text-area-buttons" },
    {
      label: "Underline",
      style: "UNDERLINE",
      className: "aw-text-area-buttons"
    }
  ]
};
class RichTextMarkdown extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      value:
        this.props.value === ""
          ? RichTextEditor.createEmptyValue()
          : RichTextEditor.createValueFromString(this.props.value, "html")
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value.toString("html")) {
      this.setState({
        value: nextProps.value
          ? RichTextEditor.createValueFromString(nextProps.value, "html")
          : RichTextEditor.createEmptyValue()
      });
    }
  }

  onChange = value => {
    this.setState({ value });
    this.props.onChange(value.toString("html"));
  };

  render() {
    return (
      <RichTextEditor
        toolbarConfig={toolbarConfig}
        placeholder={this.props.placeholder}
        editorClassName="aw-text-area"
        toolbarClassName="aw-text-area-buttons"
        className="aw-text-area-container"
        value={this.state.value}
        onChange={this.onChange}
      />
    );
  }
}

export default RichTextMarkdown;
