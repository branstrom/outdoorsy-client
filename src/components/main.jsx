import React, { Component } from "react";
import { Redirect, Switch, Route } from "react-router-dom";
import { connect } from "react-redux";
import background from "../img/background.jpg";
import smallBackground from "../img/small_background.jpg";
import StartPage from "../containers/start_page";
import MainApp from "../containers/main_app";
import { withRouter } from "react-router-dom";
import { isLoggedIn } from "../actions/login_actions";
import RequireAuth from "../components/auth/require_auth";
import logo from "../img/outdoorsy_logo.png";
import SignUp from "../containers/signup";
import SignIn from "../containers/signin";
import "../index.css";
import "../style_signin_page.css";

class Main extends Component {
  constructor(props) {
    super(props);
    this.props.isLoggedIn();
  }
  componentDidUpdate(prevProps) {
    if (!prevProps.authenticated && this.props.authenticated) {
      console.log(this.props);
      if (this.props.location.state) {
        this.props.history.push(this.props.location.state.pathname);
      } else {
        this.props.history.push("/app/activities");
      }
    }
  }

  render() {
    if (this.props.validating) {
      return <div />;
    }
    return (
      <div
        className="main-background-small"
        style={{ backgroundImage: `url(${smallBackground})` }}
      >
        <div
          className="main-background"
          style={{ backgroundImage: `url(${background})` }}
        >
          <Route exact path="/" component={SignIn} />
          <Route path="/signup" component={SignUp} />
          <Route path="/app" component={RequireAuth(MainApp)} />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated,
    validating: state.auth.validating
  };
}
const mapDispatchToProps = dispatch => {
  return {
    isLoggedIn: (...props) => dispatch(isLoggedIn(...props))
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));
