import React, { Component } from "react";
import { Button } from "./elements/button";
import AddActivity from "./elements/add_activity";
import DraggableActivity from "./elements/draggable_activity";

class ActivityList extends Component {
  constructor(props) {
    super(props);
    this.toggleSessionModal = this.toggleSessionModal.bind(this);
    this.toggleActivityModal = this.toggleActivityModal.bind(this);
  }

  toggleActivity() {
    this.props.toggleSessionModal();
  }

  toggleSessionModal(id) {
    this.props.toggleSessionModal(id, null);
  }

  toggleActivityModal(id) {
    this.props.toggleActivityModal(id);
  }

  render() {
    const { activities } = this.props;
    return (
      <div className="aw-activity-list-container">
        <div className="aw-activity-list-header">Activity Manager</div>
        <div className="margin-lg">
          <ul className="list-group">
            {activities.map(activity => {
              return (
                <li key={activity.id} className="list-group-item">
                  <a onClick={() => this.toggleActivityModal(activity.id)}>
                    <DraggableActivity
                      name={activity.activity_name}
                      id={activity.id}
                      color={activity.color}
                      toggleSessionModal={this.toggleSessionModal}
                    />
                  </a>
                </li>
              );
            })}
          </ul>
          <AddActivity onClick={() => this.props.toggleActivityModal()} />
        </div>
      </div>
    );
  }
}

export default ActivityList;
