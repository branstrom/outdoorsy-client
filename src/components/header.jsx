import React from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { slide as Menu } from "react-burger-menu";
import logo from "../img/outdoorsy.png";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
    this.toggleMenu = this.toggleMenu.bind(this);
  }
  // TODO make action of this
  handleLogout() {
    this.props.logout();
    // localStorage.removeItem("token");
    // var auth2 = window.gapi.auth2.getAuthInstance();
    // auth2.signOut().then(() => {
    //   console.log("User signed out.");
    // });
  }
  toggleMenu() {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    const { isOpen } = this.state;
    let styles = {
      bmBurgerButton: {
        position: "fixed",
        overflowY: "hidden",
        color: "white",
        height: "17px",
        width: "20px",
        fontSize: "18pt",
        fontWeight: "bold",
        right: "72px",
        top: "72px"
      },
      bmBurgerBars: {
        background: this.state.isOpen ? "transparent" : "white"
      },
      bmCrossButton: {
        height: "24px",
        width: "24px"
      },
      bmCross: {
        background: "#bdc3c7"
      },
      bmMenu: {
        background: "rgba(0, 0, 0, 0.8)",
        padding: "2.5em 1.5em 0",
        fontSize: "1.15em"
      },
      bmMorphShape: {
        fill: "#373a47"
      },
      bmItemList: {
        color: "#b8b7ad",
        padding: "0.8em"
      },
      bmOverlay: {
        background: "rgba(0, 0, 0, 0.8)"
      }
    };
    console.log(styles);

    return (
      <div>
        <Menu
          width={280}
          styles={styles}
          right={true}
          isOpen={this.state.isOpen}
          onStateChange={this.toggleMenu}
        >
          <Link
            to="/app/activities"
            className="menu-item"
            onClick={this.toggleMenu}
          >
            My Activities
          </Link>

          {/* <Link to="/app/account" className="menu-item">
            Edit Profile
          </Link>
          <Link to="/app/profile" className="menu-item">
            Account Settings
          </Link> */}
          <a
            onClick={this.handleLogout.bind(this)}
            className="menu-item"
            href="#"
          >
            Sign out
          </a>
        </Menu>
        <div className="aw-header-name">{this.getLoggedInUserUsername()}</div>
        <div>
          <Link className="" to="/app/activities">
            <img src={logo} alt="Outdoorsy" className="logo" />
          </Link>
        </div>
      </div>
    );
  }

  getLoggedInUserUsername() {
    return this.props.authenticated ? this.props.name : "NOT LOGGED IN";
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated,
    name: state.auth.name
  };
}

export default withRouter(connect(mapStateToProps)(Header));
