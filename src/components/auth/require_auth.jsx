import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

export default function(ComposedComponent) {
  class RequireAuth extends Component {
    checkAuth() {
      if (!this.props.authenticated) {
        console.log(this.props);
        this.props.history.push("/", this.props.location);
        console.log("# RequireAuth checkAuth NOT AUTH");
      }
    }

    componentWillMount() {
      this.checkAuth();
    }

    componentWillUpdate() {
      this.checkAuth();
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }

  function mapStateToProps(state) {
    return { authenticated: state.auth.authenticated };
  }

  return withRouter(connect(mapStateToProps)(RequireAuth));
}
