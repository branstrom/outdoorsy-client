import React, { Component } from "react";
import PropTypes from "prop-types";
import { DropTarget } from "react-dnd";

const type = "activity";
const spec = {
  drop() {
    return { name: "Dustbin" };
  }
};
const collect = (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
});

class DraggableZone extends Component {
  static propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired
  };

  render() {
    console.log(this.props);
    const { canDrop, connectDropTarget, noSessions } = this.props;
    const isActive = canDrop;
    console.log(isActive);
    return connectDropTarget(
      <div>
        <div className="aw-draggable-zone">
          {isActive || noSessions ? "Drag activity here to create session" : ""}
        </div>
        {this.props.children}
      </div>
    );
  }
}
export default DropTarget(type, spec, collect)(DraggableZone);
