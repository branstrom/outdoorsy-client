import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import { BrowserRouter, Route } from "react-router-dom";
import { DragDropContextProvider } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";
import reducers from "./reducers";
import promise from "redux-promise";
import reduxThunk from "redux-thunk";
//import registerServiceWorker from "./registerServiceWorker";
import axios from "axios";
import "./index.css";
import "./style_signin_page.css";
import "./toggle.css";
import { AUTH_USER } from "./actions/types";
import Main from "./components/main";
import withTracker from './components/withTracker';
console.log("PRODUCTION: ", process.env.PRODUCTION);
axios.defaults.withCredentials = true;

const createStoreWithMiddleware = applyMiddleware(promise, reduxThunk)(
  createStore
);

const store = createStoreWithMiddleware(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const token = localStorage.getItem("token");
if (token) {
  console.info("### index.js: User already logged in.");
  store.dispatch({ type: AUTH_USER, payload: token });
} else {
  console.info("### index.js: User NOT logged in.");
}

ReactDOM.render(
  <Provider store={store}>
    <DragDropContextProvider backend={HTML5Backend}>
      <BrowserRouter>
        <Route path="/" component={process.env.REACT_APP_PRODUCTION === "true" ? withTracker(Main) : Main} />
      </BrowserRouter>
    </DragDropContextProvider>
  </Provider>,
  document.getElementById("root")
);
