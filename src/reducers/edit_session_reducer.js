import {
  FETCH_SESSION,
  FETCH_SESSION_SUCCESS,
  FETCH_SESSION_FAIL,
  CLEAR_EDIT_SESSION,
  TOGGLE_SESSION_MODAL,
  FETCH_GUIDES_SUCCESS
} from "../actions/types";
import moment from "moment";
import dateformat from "dateformat";

const initialState = {
  start_date: "",
  end_date: "",
  start_time: "",
  end_time: "",
  amount: "",
  currency: "SEK",
  max_participants: "",
  min_participants: "",
  activity_point_id: null,
  locations: [],
  additional_info: "",
  guide_id: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_SESSION:
      return { ...state };
    case FETCH_SESSION_SUCCESS:
      const payload = action.payload;
      return {
        ...state,
        start_date: dateformat(
          payload.session_start_datetime,
          "yyyy-mm-dd",
          true
        ),
        end_date: dateformat(payload.session_end_datetime, "yyyy-mm-dd", true),
        start_time: dateformat(payload.session_start_datetime, "HH:MM", true),
        end_time: dateformat(payload.session_end_datetime, "HH:MM", true),
        amount: payload.session_price_amount,
        currency: payload.session_price_currency,
        max_participants: payload.session_max_participants,
        min_participants: payload.session_min_participants,
        activity_point_id: payload.session_activity_point_id,
        additional_info: payload.session_additional_info,
        guide_id: payload.guide_id,
        locations: payload.locations.map(location => {
          return {
            text: location.directions,
            value: location.location_id,
            time_of_day: location.time_of_day
          };
        })
      };
    case FETCH_SESSION_FAIL:
      return { ...state };

    case TOGGLE_SESSION_MODAL:
      console.log("reducer: ", action.payload);
      if (!action.payload.session_id) {
        console.log("returning inital state");
        return initialState;
      } else {
        return state;
      }
    case FETCH_GUIDES_SUCCESS:
      const guide_id =
        action.payload.length === 1 ? action.payload[0].value : null;
      return { ...state, guide_id };
    case CLEAR_EDIT_SESSION:
      return initialState;
    default:
      return state;
  }
}
