import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import AuthReducer from "./auth_reducer";
import ActivitiesReducer from "./activities_reducer";
import SessionsReducer from "./sessions_reducer";
import EditActivity from "./edit_activity";
import EditSessionReducer from "./edit_session_reducer";

const rootReducer = combineReducers({
  form: formReducer,
  auth: AuthReducer,
  activities: ActivitiesReducer,
  session: SessionsReducer,
  editActivity: EditActivity,
  editSession: EditSessionReducer
});

export default rootReducer;
