import {
  AUTH_USER,
  AUTH_USER_SUCCESS,
  AUTH_USER_FAIL,
  VALIDATE_USER,
  VALIDATE_USER_SUCCESS,
  VALIDATE_USER_FAIL,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER,
  SIGNUP_USER,
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAIL
} from "../actions/types";

const initialState = {
  loading: false,
  validating: false,
  signupSuccess: false,
  authenticated: false,
  name: "",
  organizationId: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case AUTH_USER:
      return { ...state, loading: true };
    case AUTH_USER_SUCCESS:
      return {
        ...state,
        authenticated: true,
        loading: false,
        name: action.payload.name,
        organizationId: action.payload.organizationId
      };
    case AUTH_USER_FAIL:
      return { ...state, loading: false, error: action.payload };
    case VALIDATE_USER:
      return { ...state, validating: true };
    case VALIDATE_USER_SUCCESS:
      return {
        ...state,
        authenticated: true,
        validating: false,
        name: action.payload.name,
        organizationId: action.payload.organizationId
      };
    case VALIDATE_USER_FAIL:
      return { ...state, validating: false, error: action.payload };
    case LOGOUT_USER:
      return { ...state, authenticated: false, loading: true };
    case LOGOUT_USER_SUCCESS:
      return { ...state, authenticated: false, loading: false };
    case SIGNUP_USER:
      return { ...state, loading: true };
    case SIGNUP_USER_SUCCESS:
      return { ...state, loading: false, signupSuccess: true };
    case SIGNUP_USER_FAIL:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
}
