import {
  EDIT_SESSION,
  EDIT_SESSION_SUCCESS,
  EDIT_SESSION_FAIL,
  CREATE_SESSION,
  CREATE_SESSION_SUCCESS,
  CREATE_SESSION_FAIL,
  TOGGLE_SESSION_MODAL,
  FETCH_GUIDES,
  FETCH_GUIDES_SUCCESS,
  FETCH_GUIDES_FAIL,
  FETCH_ACTIVIVTY_LOCATION,
  FETCH_ACTIVIVTY_LOCATION_SUCCESS,
  FETCH_ACTIVIVTY_LOCATION_FAIL,
  REMOVE_SESSION,
  REMOVE_SESSION_SUCCESS,
  REMOVE_SESSION_FAIL
} from "../actions/types";

const initialState = {
  sessionModal: false,
  editing: false,
  removing: false,
  error: null,
  activity_name: null,
  guides: [],
  activityPoints: [],
  meetingPoints: [],
  fetchingGuides: false,
  fetchingActivity: false,
  activity_id: null,
  modalSessionId: null,
  creating: false,
  session: {
    start_datetime: [],
    end_datetime: [],
    max_participants: null,
    min_participants: null,
    additional_info: null,
    activity_id: null,
    status: null,
    meeting_point_id: null,
    activity_point_id: null
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_SESSION_MODAL:
      return {
        ...state,
        sessionModal: !state.sessionModal,
        activity_id: action.payload.activity_id,
        modalSessionId: action.payload.session_id
      };
    case CREATE_SESSION:
      return { ...state, creating: true, error: null };
    case CREATE_SESSION_SUCCESS:
      const values = action.payload;
      //TODO add all locaitons aswell. and status
      return {
        ...state,
        creating: false,
        session: {
          ...state.session,
          start_datetime: values.session_start_datetime,
          end_datetime: values.session_end_datetime,
          max_participants: values.session_max_participants,
          min_participants: values.session_min_participants,
          additional_info: values.session_additional_info
        },
        modalSessionId: null,
        sessionModal: false
      };
    case CREATE_SESSION_FAIL:
      return { ...state, creating: false, error: action.payload };
    case EDIT_SESSION:
      return { ...state, editing: true, error: null };
    case EDIT_SESSION_SUCCESS:
      return { ...state, editing: false };
    case EDIT_SESSION_FAIL:
      return { ...state, editing: false, error: action.payload };
    case FETCH_GUIDES:
      return { ...state, fetchingGuides: true, error: null };
    case FETCH_GUIDES_SUCCESS:
      return { ...state, fetchingGuides: false, guides: action.payload };
    case FETCH_GUIDES_FAIL:
      return { ...state, fetchingGuides: false, error: action.payload };
    case FETCH_ACTIVIVTY_LOCATION:
      return { ...state, fetchingActivity: true, error: null };
    case FETCH_ACTIVIVTY_LOCATION_SUCCESS:
      return {
        ...state,
        fetchingActivity: false,
        activityPoints: action.payload.activityPoints,
        meetingPoints: action.payload.meetingPoints
      };
    case FETCH_ACTIVIVTY_LOCATION_FAIL:
      return { ...state, fetchingActivity: false, error: action.payload };
    case REMOVE_SESSION_SUCCESS:
      return {
        ...state,
        sessionModal: false,
        modalSessionId: null
      };
    default:
      return state;
  }
}
