import {
  FETCH_ACTIVITY,
  FETCH_ACTIVITY_SUCCESS,
  FETCH_ACTIVITY_FAIL,
  CLEAR_EDIT_ACTIVITY,
  TOGGLE_ADD_ACTIVITY_MODAL
} from "../actions/types";

const initialState = {
  activity_name: "",
  activity_type: "",
  level: "",
  detailed_description: "",
  description: "",
  media: null,
  image_upload_0: null,
  image_upload_1: null,
  image_upload_2: null,
  image_upload_3: null,
  image_upload_4: null,
  image_upload_5: null,
  image_upload_6: null,
  locations: null,
  activity_point_description: [],
  meeting_point_description: [],
  includedText: "",
  items: null,
  included: [],
  required: [],
  requiredText: "",
  color: "red"
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_ACTIVITY:
      return { ...state };
    case FETCH_ACTIVITY_SUCCESS:
      const payload = action.payload;
      return {
        ...state,
        activity_name: payload.activity_name,
        activity_type: payload.activity_type_name,
        activity_category: payload.category,
        description: payload.description,
        detailed_description: payload.detailed_description,
        level: payload.level.toUpperCase(),
        color: payload.color,
        locations: payload.locations,
        activity_point_description: payload.locations
          .filter(point => {
            return point.location_type === "Activity point";
          })
          .map(point => {
            console.log(point);
            return {
              id: point.id,
              directions: point.directions,
              coordinates: point.coordinates,
              is_active: 1,
              location_type: point.location_type
            };
          }),
        meeting_point_description: payload.locations
          .filter(point => {
            return point.location_type === "Meeting point";
          })
          .map(point => {
            return {
              id: point.id,
              directions: point.directions,
              coordinates: point.coordinates,
              is_active: 1,
              location_type: point.location_type
            };
          }),
        items: payload.items,
        included: payload.items
          .filter(item => {
            return item.item_type === "Included";
          })
          .map(item => {
            return { name: item.name, id: item.id };
          }),
        required: payload.items
          .filter(item => {
            return item.item_type === "Required";
          })
          .map(item => {
            return { name: item.name, id: item.id };
          }),
        media: payload.media,
        image_upload_0: payload.media[0]
          ? { id: payload.media[0].id, preview: payload.media[0].media_url }
          : null,
        image_upload_1: payload.media[1]
          ? { id: payload.media[1].id, preview: payload.media[1].media_url }
          : null,
        image_upload_2: payload.media[2]
          ? { id: payload.media[2].id, preview: payload.media[2].media_url }
          : null,
        image_upload_3: payload.media[3]
          ? { id: payload.media[3].id, preview: payload.media[3].media_url }
          : null,
        image_upload_4: payload.media[4]
          ? { id: payload.media[4].id, preview: payload.media[4].media_url }
          : null,
        image_upload_5: payload.media[5]
          ? { id: payload.media[5].id, preview: payload.media[5].media_url }
          : null,
        image_upload_6: payload.media[6]
          ? { id: payload.media[6].id, preview: payload.media[6].media_url }
          : null
      };
    case FETCH_ACTIVITY_FAIL:
      return { ...state };
    case TOGGLE_ADD_ACTIVITY_MODAL:
      if (action.payload === undefined) {
        return initialState;
      } else {
        return state;
      }
    default:
      return state;
  }
}
