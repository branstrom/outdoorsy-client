import {
  TOGGLE_ADD_ACTIVITY_MODAL,
  TOGGLE_SESSION_MODAL,
  SET_ACTIVITY_ID,
  SET_SESSION_ID,
  CREATE_ACTIVITY,
  CREATE_ACTIVITY_SUCCESS,
  CREATE_ACTIVITY_FAIL,
  CREATE_ACTIVITY_ERRORS,
  UPDATE_ACTIVITY,
  UPDATE_ACTIVITY_SUCCESS,
  UPDATE_ACTIVITY_FAIL,
  FETCH_ACTIVITIES,
  FETCH_ACTIVITIES_SUCCESS,
  FETCH_ACTIVITIES_FAIL,
  FETCH_ALL_ACTIVITIES,
  FETCH_ALL_ACTIVITIES_SUCCESS,
  FETCH_ALL_ACTIVITIES_FAIL,
  FETCH_ACTIVITY_TYPES,
  FETCH_ACTIVITY_TYPES_SUCCESS,
  FETCH_ACTIVITY_TYPES_FAIL,
  REMOVE_ACTIVITY_SUCCESS,
  REMOVE_ACTIVITY_FAIL,
  REMOVE_ACTIVITY,
  REMOVE_SESSION,
  REMOVE_SESSION_SUCCESS,
  REMOVE_SESSION_FAIL
} from "../actions/types";

const initialState = {
  addActivityModal: false,
  modalActivityId: null,
  fetching: false,
  fetchingAll: false,
  creating: false,
  updating: false,
  error: null,
  errors: null,
  activities: {},
  allActivities: [],
  activityId: null,
  sessionId: null,
  types: [],
  categories: []
};

function removeByKey(myObj, deleteKey) {
  return Object.keys(myObj)
    .filter(key => key != deleteKey)
    .reduce((result, current) => {
      result[current] = myObj[current];
      return result;
    }, {});
}

export default function(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_ADD_ACTIVITY_MODAL:
      if (!state.addActivityModal) {
        return {
          ...state,
          addActivityModal: true,
          modalActivityId: action.payload
        };
      } else {
        return { ...state, addActivityModal: false, modalActivityId: null };
      }
    case TOGGLE_SESSION_MODAL: {
      return { ...state, sessionId: null };
    }
    case SET_ACTIVITY_ID:
      return { ...state, activityId: action.payload };
    case SET_SESSION_ID:
      return { ...state, sessionId: action.payload };
    case CREATE_ACTIVITY:
      return { ...state, creating: true, error: null, errors: null };
    case CREATE_ACTIVITY_SUCCESS:
      newAll = [...state.allActivities, action.payload];
      newAll = newAll.sort((a, b) => {
        const aName = a.activity_name.toLowerCase();
        const bName = b.activity_name.toLowerCase();
        if (aName < bName) return -1;
        if (aName > bName) return 1;
        return 0;
      });
      return {
        ...state,
        creating: false,
        allActivities: newAll,
        addActivityModal: false
      };
    case CREATE_ACTIVITY_FAIL:
      return { ...state, creating: false, error: action.payload };
    case UPDATE_ACTIVITY:
      return { ...state, updating: true, error: null, errors: null };
    case UPDATE_ACTIVITY_SUCCESS:
      console.log(action.payload);
      let index = state.allActivities.findIndex(activity => {
        return activity.id === action.payload.id;
      });
      console.log(index);
      let newAll = Object.assign(state.allActivities);
      console.log(newAll);
      newAll[index].color = action.payload.color;
      console.log(newAll);
      let newActivities = Object.assign(state.activities);
      if (newActivities[index]) {
        newActivities[index].color = action.payload.color;
      }
      return {
        ...state,
        updating: false,
        allActivities: newAll,
        activities: newActivities
      };
    case UPDATE_ACTIVITY_FAIL:
      return { ...state, updating: false, error: action.payload };
    case FETCH_ACTIVITIES:
      return { ...state, fetching: true, error: null };
    case FETCH_ACTIVITIES_SUCCESS:
      let activities = {};
      console.log(action.payload);
      action.payload.map(row => {
        if (!activities[row.activity_id]) {
          activities[row.activity_id] = {
            id: row.activity_id,
            name: row.activity_name,
            level: row.activity_level,
            type_name: row.activity_type_name,
            type_category_name: row.activity_type_category_name,
            meeting_point_coordinates: row.meeting_point_coordinates,
            meeting_point_directions: row.meeting_point_directions,
            point_coordinates: row.activity_point_coordinates,
            point_directions: row.activity_point_directions,
            color: row.activity_color,
            booked: 0,
            max_booked: 0,
            sales: 0,
            sessions: {}
          };
        }
        if (!activities[row.activity_id].sessions[row.session_id]) {
          console.log(row.session_id);
          activities[row.activity_id].sessions[row.session_id] = {
            id: row.session_id,
            created_at: row.session_created_at,
            updated_at: row.session_updated_at,
            start_datetime: row.session_start_datetime,
            end_datetime: row.session_end_datetime,
            max_participants: row.session_max_participants,
            min_participants: row.session_min_participants,
            additional_info: row.session_additional_info,
            meeting_point_id: row.session_meeting_point_id,
            activity_point_id: row.session_activity_point_id,
            price: row.session_price_amount,
            guide: row.guide_first_name + " " + row.guide_last_name,
            booked: 0,
            participants: {}
          };
          activities[row.activity_id].max_booked +=
            row.session_max_participants;
        }
        if (
          !activities[row.activity_id].sessions[row.session_id].participants[
            row.participant_id
          ] &&
          row.participant_id != null &&
          row.booking_status !== "Canceled"
        ) {
          activities[row.activity_id].sessions[row.session_id].participants[
            row.participant_id
          ] = {
            id: row.participant_id,
            first_name: row.participant_firstname,
            last_name: row.participant_lastname,
            age: row.participant_age,
            mobile_number: row.participant_mobile_number,
            email: row.participant_email,
            gender: row.participant_gender,
            height: row.participant_height,
            weight: row.participant_weight
          };
          activities[row.activity_id].sessions[row.session_id].booked += 1;
          activities[row.activity_id].booked += 1;
          activities[row.activity_id].sales += row.session_price_amount;
        }
      });
      return { ...state, fetching: false, activities };
    case FETCH_ACTIVITIES_FAIL:
      return { ...state, fetching: false, error: action.payload };
    case FETCH_ALL_ACTIVITIES:
      return { ...state, fetchingAll: true, error: null };
    case FETCH_ALL_ACTIVITIES_SUCCESS:
      return { ...state, fetchingAll: false, allActivities: action.payload };
    case FETCH_ALL_ACTIVITIES_FAIL:
      return { ...state, fetchingAll: false, error: action.payload };
    case REMOVE_ACTIVITY:
      return { ...state, removing: true, error: null };
    case REMOVE_ACTIVITY_SUCCESS:
      const { id } = action.payload;
      return {
        ...state,
        removing: false,
        allActivities: state.allActivities.filter(obj => obj.id != id),
        activities: removeByKey(state.activities, id),
        addActivityModal: false,
        modalActivityId: null
      };
    case REMOVE_ACTIVITY_FAIL:
      return { ...state, removing: false, error: action.payload };
    case REMOVE_SESSION:
      return { ...state, removing: true, error: null };
    case REMOVE_SESSION_SUCCESS:
      console.log(state.activities[state.activityId]);
      const sessions = removeByKey(
        state.activities[state.activityId].sessions,
        action.payload.id
      );
      let activityId = state.activityId;
      console.log("SEssions: ", sessions);
      if (Object.keys(sessions).length === 0) {
        activities = removeByKey(state.activities, state.activityId);
        activityId = null;
      } else {
        activities = {
          ...state.activities,
          [state.activityId]: {
            ...state.activities[state.activityId],
            sessions
          }
        };
      }
      return {
        ...state,
        removing: false,
        activities,
        addActivityModal: false,
        modalActivityId: null,
        activityId,
        sessionId: null
      };
    case CREATE_ACTIVITY_ERRORS:
      return { ...state, errors: action.payload };
    case FETCH_ACTIVITY_TYPES:
      return { ...state };
    case FETCH_ACTIVITY_TYPES_SUCCESS:
      let categories = {};
      action.payload.forEach(type => {
        categories[type.category] = {
          id: type.id,
          text: type.category,
          value: type.category
        };
      });
      categories = Object.values(categories);
      return { ...state, types: action.payload, categories };
    case FETCH_ACTIVITY_TYPES_FAIL:
      return { ...state, error: action.payload };
    default:
      return state;
  }
}
